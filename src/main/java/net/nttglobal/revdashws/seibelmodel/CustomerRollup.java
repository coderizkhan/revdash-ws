package net.nttglobal.revdashws.seibelmodel;

import javax.persistence.*;

@Entity
public class CustomerRollup {

	@Id
	private String rollup;

	public String getRollup() {
		return rollup;
	}

	public void setRollup(String rollup) {
		this.rollup = rollup;
	}

	public CustomerRollup() {
		super();
	}
	
}
