package net.nttglobal.revdashws.seibelmodel;

import javax.persistence.*;

@Entity
public class ActiveMrr {

	@Id
	private int gTotal;

	private String cust;

	public int getgTotal() {
		return gTotal;
	}

	public void setgTotal(int gTotal) {
		this.gTotal = gTotal;
	}

	public String getCust() {
		return cust;
	}

	public void setCust(String cust) {
		this.cust = cust;
	}

	public ActiveMrr() {
		super();
	}

}
