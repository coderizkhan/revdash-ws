package net.nttglobal.revdashws.seibelmodel;

import javax.persistence.*;

@Entity
public class TermStatus {

	@Id
	private int gTotal;
	
	private String termstatus;

	public int getgTotal() {
		return gTotal;
	}

	public void setgTotal(int gTotal) {
		this.gTotal = gTotal;
	}

	public String getTermStatus() {
		return termstatus;
	}

	public void setTermStatus(String termStatus) {
		this.termstatus = termStatus;
	}

	public TermStatus() {
		super();
	}
	
	
}
