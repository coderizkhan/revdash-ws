package net.nttglobal.revdashws.seibelmodel;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.*;

/**
 *
 * @author rizwan
 */
@Entity
public class SidInfo {

    @Id
    private String SID;
    private String PARENTACCOUNTNAME;
    private String VLID;
    private String LOCATION_NAME;
    private String X_NTT_W_NUMBER;
    private String SERVICEINSTANCENAME;
    private String SERVICE_TYPE;
    private String SID_CREATED;
    private String ORDER_TERM;
    private String MRR;
    private String NRR;
    private String BSD;
    private String BED;
    private String OED;
    private String PROVIDER;
    private String RENEWAL_START_DT;
    private String RENEWAL_TERM;
    private String RENEWAL_END_DATE;

    public String getSID() {
        return SID;
    }

    public void setSID(String sID) {
        SID = sID;
    }

    public String getPARENTACCOUNTNAME() {
        return PARENTACCOUNTNAME;
    }

    public void setPARENTACCOUNTNAME(String pARENTACCOUNTNAME) {
        PARENTACCOUNTNAME = pARENTACCOUNTNAME;
    }

    public String getVLID() {
        return VLID;
    }

    public void setVLID(String vLID) {
        VLID = vLID;
    }

    public String getLOCATION_NAME() {
        return LOCATION_NAME;
    }

    public void setLOCATION_NAME(String lOCATION_NAME) {
        LOCATION_NAME = lOCATION_NAME;
    }

    public String getX_NTT_W_NUMBER() {
        return X_NTT_W_NUMBER;
    }

    public void setX_NTT_W_NUMBER(String x_NTT_W_NUMBER) {
        X_NTT_W_NUMBER = x_NTT_W_NUMBER;
    }

    public String getSERVICEINSTANCENAME() {
        return SERVICEINSTANCENAME;
    }

    public void setSERVICEINSTANCENAME(String sERVICEINSTANCENAME) {
        SERVICEINSTANCENAME = sERVICEINSTANCENAME;
    }

    public String getSERVICE_TYPE() {
        return SERVICE_TYPE;
    }

    public void setSERVICE_TYPE(String sERVICE_TYPE) {
        SERVICE_TYPE = sERVICE_TYPE;
    }

    public String getSID_CREATED() {
        return SID_CREATED.substring(0, 10);
    }

    public void setSID_CREATED(String sID_CREATED) {
        SID_CREATED = sID_CREATED;
    }

    public String getORDER_TERM() {
        return ORDER_TERM;
    }

    public void setORDER_TERM(String oRDER_TERM) {
        ORDER_TERM = oRDER_TERM;
    }

    public String getMRR() {
        return MRR;
    }

    public void setMRR(String mRR) {
        MRR = mRR;
    }

    public String getNRR() {
        return NRR;
    }

    public void setNRR(String nRR) {
        NRR = nRR;
    }

    public String getBSD() {
        return BSD;
    }

    public void setBSD(String bSD) {
        BSD = bSD;
    }

    public String getBED() {
        return BED;
    }

    public void setBED(String bED) {
        BED = bED;
    }

    public String getOED() {
        return OED;
    }

    public void setOED(String oED) {
        OED = oED;
    }

    public String getPROVIDER() {
        return PROVIDER;
    }

    public void setPROVIDER(String pROVIDER) {
        PROVIDER = pROVIDER;
    }

    public String getRENEWAL_START_DT() {
        return RENEWAL_START_DT;
    }

    public void setRENEWAL_START_DT(String rENEWAL_START_DT) {
        RENEWAL_START_DT = rENEWAL_START_DT;
    }

    public String getRENEWAL_TERM() {
        return RENEWAL_TERM;
    }

    public void setRENEWAL_TERM(String rENEWAL_TERM) {
        RENEWAL_TERM = rENEWAL_TERM;
    }

    public String getRENEWAL_END_DATE() {
        return RENEWAL_END_DATE;
    }

    public void setRENEWAL_END_DATE(String rENEWAL_END_DATE) {
        RENEWAL_END_DATE = rENEWAL_END_DATE;
    }

    public SidInfo() {
        super();
    }

}
