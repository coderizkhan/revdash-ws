package net.nttglobal.revdashws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import net.nttglobal.revdashws.model.ActionOnEtl;
import net.nttglobal.revdashws.service.ActionOnEtlService;

@RestController
public class ActionOnEtlController {

	@Autowired
	private ActionOnEtlService actionService;

	@PostMapping("/etlData/{etlCode}/action")
	public ResponseEntity<ActionOnEtl> createAction(@PathVariable(value = "etlCode") String etlCode,
			@RequestBody ActionOnEtl action) {
		final ActionOnEtl actionData = this.actionService.addAction(etlCode, action);
		return ResponseEntity.status(HttpStatus.CREATED).body(actionData);
	}


}
