package net.nttglobal.revdashws.controller;

import java.util.List;
import net.nttglobal.revdashws.model.CustomerContract;
import net.nttglobal.revdashws.service.CustomerContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/customercontract")
@CrossOrigin(origins = "http://localhost:8081")
public class CustomerContractController {
    
    @Value("${revenue.dashboard.contract.template.uploads}")
    private String uploadPath;
    
    @Autowired
    private CustomerContractService custContServ;

    @GetMapping("/count")
    public ResponseEntity<Long> count() {
        final Long custContCount = this.custContServ.countCustomerContract();
        return ResponseEntity.status(HttpStatus.OK).body(custContCount);
    }

    @PostMapping("/add")
    public ResponseEntity<String> add(@RequestBody CustomerContract contract, @RequestParam("file") MultipartFile file) {
        custContServ.addCustContract(contract, file);
        return ResponseEntity.status(HttpStatus.CREATED).body("Record added successfully !!!");
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<CustomerContract>> view() {
        final List<CustomerContract> contractList = custContServ.viewCustContract();
        return ResponseEntity.status(HttpStatus.OK).body(contractList);
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<CustomerContract> viewById(@PathVariable Long id) {
        final CustomerContract custContract = custContServ.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(custContract);
    }

    @GetMapping("/getByName/{customerName}")
    public ResponseEntity<CustomerContract> viewByName(@PathVariable String customerName) {
        final CustomerContract customer = custContServ.getByCustName(customerName);
        return ResponseEntity.status(HttpStatus.OK).body(customer);
    }

    @PutMapping("/{id}/update")
    public ResponseEntity<String> update(@PathVariable Long id, @RequestBody CustomerContract contract) {
        this.custContServ.editContract(id, contract);
        return ResponseEntity.status(HttpStatus.OK).body("Records Updated !!");
    }

    @DeleteMapping("{id}/delete")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        this.custContServ.deleteCustContract(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    
    @GetMapping("/customer-list")
    public ResponseEntity<List<String>> getCustomerList(){
    	List<String> list = this.custContServ.ListCustomerContract();
    	return ResponseEntity.status(HttpStatus.OK).body(list);
    }
 
}
