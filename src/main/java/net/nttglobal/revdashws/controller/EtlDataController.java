package net.nttglobal.revdashws.controller;

import java.text.ParseException;
import java.util.List;
import net.nttglobal.revdashws.dto.EtlDataDto;
import net.nttglobal.revdashws.dto.EtlDataParamDto;
import net.nttglobal.revdashws.dto.EtlUserInfo;
import net.nttglobal.revdashws.model.EtlData;
import net.nttglobal.revdashws.model.EtlDates;
import net.nttglobal.revdashws.service.EtlDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author rizwan
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/etldata")
//@CrossOrigin(origins = "http://localhost:8081")
public class EtlDataController {

    @Autowired
    private EtlDataService service;

    @PostMapping("/add")
    public ResponseEntity<List<EtlDataDto>> add(@RequestBody List<EtlDataParamDto> dto, @RequestParam("file") MultipartFile[] files) throws ParseException {
        final List<EtlDataDto> etlDataDto = this.service.addEtlData(dto, files);
        return ResponseEntity.status(HttpStatus.CREATED).body(etlDataDto);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<EtlData>> allRecords() {
        final List<EtlData> etlData = this.service.getAllEtl();
        return ResponseEntity.status(HttpStatus.OK).body(etlData);
    }

    @GetMapping("/{etlCode}/getByEtlCode")
    public ResponseEntity<List<EtlData>> getByEtlCode(@PathVariable String etlCode) {
        final List<EtlData> etlDataList = this.service.viewByEtlCode(etlCode);
        return ResponseEntity.status(HttpStatus.OK).body(etlDataList);
    }

    @GetMapping("/{sid}/getBySid")
    public ResponseEntity<EtlData> getBySid(@PathVariable String sid) {
        final EtlData etlData = this.service.getEtlDataBySid(sid);
        return ResponseEntity.status(HttpStatus.OK).body(etlData);
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        this.service.delete(id);
        return ResponseEntity.status(HttpStatus.GONE).build();
    }

    @GetMapping("/getEtlCalculatedData")
    public ResponseEntity<EtlDataDto>
            getEtlCalculatedData(@RequestBody EtlUserInfo userEtlInfo) throws
            ParseException {
        final EtlDataDto dto
                = this.service.etlCalculatedData(userEtlInfo);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PostMapping("/etldates")
    public ResponseEntity<EtlDates>
            getEtlDates(@RequestBody EtlUserInfo userEtlInfo) throws ParseException {
        final EtlDates finalDates = this.service.calcEtlDates(userEtlInfo);
        return ResponseEntity.status(HttpStatus.OK).body(finalDates);
    }

}
