package net.nttglobal.revdashws.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.nttglobal.revdashws.model.ActionOnEtl;

@Repository
public interface ActionOnEtlRepository extends JpaRepository<ActionOnEtl, Long>{

}
