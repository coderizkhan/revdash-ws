package net.nttglobal.revdashws.repository;

import java.util.List;
import net.nttglobal.revdashws.model.CustomerContract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rizwan
 */
@Repository
public interface CustomerContractRepository extends JpaRepository<CustomerContract, Long> {

    @Query(value = "select * "
            + "from customer_contract "
            + "where customerName = :name "
            + "order by id desc limit 1",
            nativeQuery = true)
    public CustomerContract findByCustomerName(@Param("name") String name);
    
    @Query(value = "SELECT distinct customerName "
    		+ "FROM customer_contract",
    		nativeQuery = true)
    public List<String> CustomerContractList();

}
