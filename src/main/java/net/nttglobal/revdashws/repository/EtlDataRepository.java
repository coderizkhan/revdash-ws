package net.nttglobal.revdashws.repository;

import java.util.List;
import net.nttglobal.revdashws.model.EtlData;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author rizwan
 */
public interface EtlDataRepository extends JpaRepository<EtlData, Long> {

    List<EtlData> findByEtlCode(String etlCode);

    Long countByContractCustName(String name);

    EtlData findBySid(String sid);
    

}
