package net.nttglobal.revdashws.seibelrepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import net.nttglobal.revdashws.seibelmodel.BusinessUnit;


@Repository
public interface BusinessUnitRepository extends JpaRepository<BusinessUnit, Integer> {

	@Query(value = " SELECT "
			+" p.CUST, " 
			+" ROUND(SUM(p.MRR)/1000)  as GTOTAL " 
			+" FROM  vware4.x_siebel_orders_sid a left join " 
			+" (Select c.SERVICEINSTANCEID, SUM(c.NETTOTALMRC) AS MRR,  (case when  c.PARENTACCOUNTNAME like 'IBM%' THEN 'IBM' ELSE 'NON IBM'  END) as CUST FROM VWARE.X_SIEBEL_ORDERS c group By c.SERVICEINSTANCEID,  c.PARENTACCOUNTNAME) P " 
			+" ON a.SERVICEINSTANCEID = p.SERVICEINSTANCEID " 
			+" WHERE (a.BILLINGENDDATE is NULL OR a.BILLINGENDDATE>=sysdate) " 
			+" AND a.BILLINGSTARTDATE is not null " 
			+" AND a.SERVICEINSTANCESTATUS NOT IN ('Cancelled') " 
			+" AND a.PARENTACCOUNTNAME NOT LIKE '%CID%ACG%' " 
			+" AND p.CUST is not null " 
			+" GROUP BY p.CUST " 
			+" ORDER by GTOTAL DESC ", nativeQuery=true)
	public List<BusinessUnit> findBusinessUnitBau();
	
	@Query(value = " select * fROM ( " 
			+" Select * From ( " 
			+" SELECT " 
			+" p.CUST, " 
			+" ROUND(SUM(p.MRR)/1000) as GTOTAL " 
			+" FROM  vware4.x_siebel_orders_sid a left join " 
			+" (Select c.SERVICEINSTANCEID, SUM(c.NETTOTALMRC) AS MRR, ord.X_SALES_TERRITORY CUST  FROM VWARE.X_SIEBEL_ORDERS c left join SIEBEL.s_order ord on c.ORDERNUMBER = ord.ORDER_NUM group By c.SERVICEINSTANCEID, ord.X_SALES_TERRITORY) P " 
			+" ON a.SERVICEINSTANCEID = p.SERVICEINSTANCEID " 
			+" WHERE (a.BILLINGENDDATE is NULL OR a.BILLINGENDDATE>=sysdate) " 
			+" AND a.BILLINGSTARTDATE is not null " 
			+" AND a.SERVICEINSTANCESTATUS NOT IN ('Cancelled') " 
			+" AND a.PARENTACCOUNTNAME LIKE '%CID%ACG%' " 
			+" GROUP BY p.CUST " 
			+" ORDER by GTOTAL DESC) z " 
			+" WHERE ROWNUM<=3 " 
			+" UNION " 
			+" (Select zz.SALES_TERRITORY, SUM(zz.GTOTAL) from " 
			+" (select z.CUST, z.GTOTAL, (CASE WHEN rownum <= 3 THEN 'Top 3' ELSE 'OTHERS' END) SALES_TERRITORY " 
			+" from(SELECT p.CUST, ROUND (SUM (p.MRR) / 1000) AS GTOTAL " 
			+" FROM vware4.x_siebel_orders_sid a " 
			+" LEFT JOIN " 
			+" (SELECT c.SERVICEINSTANCEID, " 
			+" SUM (c.NETTOTALMRC) AS MRR, " 
			+" ord.X_SALES_TERRITORY CUST " 
			+" FROM VWARE.X_SIEBEL_ORDERS c " 
			+" LEFT JOIN SIEBEL.s_order ord ON c.ORDERNUMBER = ord.ORDER_NUM " 
			+" GROUP BY c.SERVICEINSTANCEID, ord.X_SALES_TERRITORY) P " 
			+" ON a.SERVICEINSTANCEID = p.SERVICEINSTANCEID " 
			+" WHERE (a.BILLINGENDDATE IS NULL OR a.BILLINGENDDATE >= SYSDATE) " 
			+" AND a.BILLINGSTARTDATE IS NOT NULL " 
			+" AND a.SERVICEINSTANCESTATUS NOT IN ('Cancelled') " 
			+" AND a.PARENTACCOUNTNAME LIKE '%CID%ACG%' " 
			+" GROUP BY p.CUST " 
			+" ORDER BY GTOTAL DESC) z) zz " 
			+" WHERE SALES_TERRITORY IN('OTHERS') " 
			+" GROUP BY SALES_TERRITORY)) FIN " 
			+" order by FIN.GTOTAL DESC ", nativeQuery=true)
	public List<BusinessUnit> findBusinessUnitNtt();
	
}
