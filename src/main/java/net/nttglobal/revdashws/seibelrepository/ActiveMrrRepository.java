package net.nttglobal.revdashws.seibelrepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import net.nttglobal.revdashws.seibelmodel.ActiveMrr;

@Repository
public interface ActiveMrrRepository extends JpaRepository<ActiveMrr, Integer> {

	@Query(value = " SELECT" + " TAB.CUST, " + " ROUND(SUM(TAB.MRR)/1000) as GTOTAL " + " FROM(" + " SELECT "
			+ " p.MRR, " + " (case when  a.PARENTACCOUNTNAME like '%CID%ACG%' THEN 'NTT' ELSE 'NGN'  END) as CUST "
			+ " FROM  vware4.x_siebel_orders_sid a left join "
			+ " (Select c.SERVICEINSTANCEID, SUM(c.NETTOTALMRC) AS MRR FROM VWARE.X_SIEBEL_ORDERS c group By c.SERVICEINSTANCEID) P "
			+ " ON a.SERVICEINSTANCEID = p.SERVICEINSTANCEID "
			+ " WHERE (a.BILLINGENDDATE is NULL OR a.BILLINGENDDATE>=sysdate) " + " AND a.BILLINGSTARTDATE is not null "
			+ " AND a.SERVICEINSTANCESTATUS NOT IN ('Cancelled') ) TAB " + " GROUP BY TAB.CUST", nativeQuery = true)
	public List<ActiveMrr> findActiveMrr();

}
