package net.nttglobal.revdashws.seibelrepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import net.nttglobal.revdashws.seibelmodel.TermStatus;


@Repository
public interface TermStatusRepository extends JpaRepository<TermStatus, Integer> {

	@Query(value = " Select  TERMSTATUS, ROUND(SUM(MRR)/1000) GTOTAL " 
					+" From (" 
					+" SELECT" 
					+" a.SERVICEINSTANCEID SID, a.PARENTACCOUNTNAME, " 
					+" a.X_TERM Order_Term, p.MRR, p. NRR, " 
					+" a.BILLINGSTARTDATE BSD, a.BILLINGENDDATE BED, " 
					+" b.X_RENEWAL_DT  RENEWAL_START_DT, " 
					+" b.X_RENEWAL_TERM RENEWAL_TERM, " 
					+" b.X_RENEWAL_END_DATE RENEWAL_END_DATE, " 
					+" (CASE WHEN  b.X_RENEWAL_END_DATE  > sysdate THEN 'IN TERM'  ELSE " 
					+" CASE WHEN  b.X_RENEWAL_END_DATE  <= sysdate THEN 'OUT TERM'  ELSE " 
					+" CASE WHEN  b.X_RENEWAL_END_DATE IS NULL THEN " 
					+" CASE WHEN a.BILLINGSTARTDATE +ROUND((a.X_TERM)*30.42) > Sysdate THEN  'IN TERM' ELSE 'OUT TERM'  END END END END) as TERMSTATUS " 
					+" FROM  vware4.x_siebel_orders_sid a left join " 
					+" (Select c.SERVICEINSTANCEID, SUM(c.NETTOTALMRC) AS MRR, SUM(c.NETTOTALNRC) AS NRR FROM VWARE.X_SIEBEL_ORDERS c group By c.SERVICEINSTANCEID) P " 
					+" ON a.SERVICEINSTANCEID = p.SERVICEINSTANCEID " 
					+" left join siebel.s_order_item b " 
					+" ON a.SERVICEINSTANCEID = b.ROOT_ORDER_ITEM_ID  and b.row_id = b.ROOT_ORDER_ITEM_ID " 
					+" Left join (SELECT  xm.par_row_id,  i.X_PAR_ROW_ID, i.X_PROVIDER, p.name Provider  FROM SIEBEL.CX_PROV_COMM_XM  xm, siebel.cx_prov_comment i, siebel.s_org_ful p  where i.ROW_ID = xm.PAR_ROW_ID and  p.row_id= i.X_PROVIDER and i.X_PROVIDER_TYPE not in('Second')) h " 
					+" ON a.SERVICEINSTANCEID = h.X_PAR_ROW_ID " 
					+" WHERE (a.BILLINGENDDATE is NULL OR a.BILLINGENDDATE>=sysdate) " 
					+" AND a.BILLINGSTARTDATE is not null " 
					+" AND a.SERVICEINSTANCESTATUS NOT IN ('Cancelled') " 
					+" AND a.PARENTACCOUNTNAME NOT LIKE '%CID%ACG%' " 
					+" Group by a.SERVICEINSTANCEID,  p.MRR, p. NRR,a.PARENTACCOUNTNAME, a.X_TERM, b.X_RENEWAL_DT, b.X_RENEWAL_TERM, b.X_RENEWAL_END_DATE, a.BILLINGSTARTDATE, a.BILLINGENDDATE ORDER by a.SERVICEINSTANCEID ) " 
					+" group by TERMSTATUS", nativeQuery=true)
	public List<TermStatus> findTermStatusBau();
	
	@Query(value = " Select  TERMSTATUS, ROUND(SUM(MRR)/1000) GTOTAL " 
			+" From (" 
			+" SELECT" 
			+" a.SERVICEINSTANCEID SID, a.PARENTACCOUNTNAME, " 
			+" a.X_TERM Order_Term, p.MRR, p. NRR, " 
			+" a.BILLINGSTARTDATE BSD, a.BILLINGENDDATE BED, " 
			+" b.X_RENEWAL_DT  RENEWAL_START_DT, " 
			+" b.X_RENEWAL_TERM RENEWAL_TERM, " 
			+" b.X_RENEWAL_END_DATE RENEWAL_END_DATE, " 
			+" (CASE WHEN  b.X_RENEWAL_END_DATE  > sysdate THEN 'IN TERM'  ELSE " 
			+" CASE WHEN  b.X_RENEWAL_END_DATE  <= sysdate THEN 'OUT TERM'  ELSE " 
			+" CASE WHEN  b.X_RENEWAL_END_DATE IS NULL THEN " 
			+" CASE WHEN a.BILLINGSTARTDATE +ROUND((a.X_TERM)*30.42) > Sysdate THEN  'IN TERM' ELSE 'OUT TERM'  END END END END) as TERMSTATUS " 
			+" FROM  vware4.x_siebel_orders_sid a left join " 
			+" (Select c.SERVICEINSTANCEID, SUM(c.NETTOTALMRC) AS MRR, SUM(c.NETTOTALNRC) AS NRR FROM VWARE.X_SIEBEL_ORDERS c group By c.SERVICEINSTANCEID) P " 
			+" ON a.SERVICEINSTANCEID = p.SERVICEINSTANCEID " 
			+" left join siebel.s_order_item b " 
			+" ON a.SERVICEINSTANCEID = b.ROOT_ORDER_ITEM_ID  and b.row_id = b.ROOT_ORDER_ITEM_ID " 
			+" Left join (SELECT  xm.par_row_id,  i.X_PAR_ROW_ID, i.X_PROVIDER, p.name Provider  FROM SIEBEL.CX_PROV_COMM_XM  xm, siebel.cx_prov_comment i, siebel.s_org_ful p  where i.ROW_ID = xm.PAR_ROW_ID and  p.row_id= i.X_PROVIDER and i.X_PROVIDER_TYPE not in('Second')) h " 
			+" ON a.SERVICEINSTANCEID = h.X_PAR_ROW_ID " 
			+" WHERE (a.BILLINGENDDATE is NULL OR a.BILLINGENDDATE>=sysdate) " 
			+" AND a.BILLINGSTARTDATE is not null " 
			+" AND a.SERVICEINSTANCESTATUS NOT IN ('Cancelled') " 
			+" AND a.PARENTACCOUNTNAME LIKE '%CID%ACG%' " 
			+" Group by a.SERVICEINSTANCEID,  p.MRR, p. NRR,a.PARENTACCOUNTNAME, a.X_TERM, b.X_RENEWAL_DT, b.X_RENEWAL_TERM, b.X_RENEWAL_END_DATE, a.BILLINGSTARTDATE, a.BILLINGENDDATE ORDER by a.SERVICEINSTANCEID ) " 
			+" group by TERMSTATUS", nativeQuery=true)
public List<TermStatus> findTermStatusNtt();
	
}
