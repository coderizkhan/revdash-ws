package net.nttglobal.revdashws.seibelrepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.nttglobal.revdashws.seibelmodel.SidInfo;

/**
 *
 * @author rizwan
 */
@Repository
public interface SidInfoRepository extends JpaRepository<SidInfo, String> {
	
    @Query(value = " SELECT "
            + " a.SERVICEINSTANCEID SID, a.PARENTACCOUNTNAME, a.VLID, a.SERVICEACCOUNTNAME Location_Name, "
            + " a.X_NTT_W_NUMBER, a.SERVICEINSTANCENAME, a.X_SERVICE_TYPE SERVICE_TYPE, b.CREATED SID_CREATED, "
            + " a.X_TERM Order_Term, p.MRR, p. NRR, "
            + " a.BILLINGSTARTDATE BSD, a.BILLINGENDDATE BED, "
            + " (a.BILLINGSTARTDATE+(a.X_TERM)*30.41667)  as OED, "
            + " h.PROVIDER, b.X_RENEWAL_DT  RENEWAL_START_DT, "
            + " b.X_RENEWAL_TERM RENEWAL_TERM, "
            + " b.X_RENEWAL_END_DATE RENEWAL_END_DATE "
            + " FROM  vware4.x_siebel_orders_sid a left join "
            + " (Select c.SERVICEINSTANCEID, SUM(c.NETTOTALMRC) AS MRR, SUM(c.NETTOTALNRC) AS NRR "
            + " FROM VWARE.X_SIEBEL_ORDERS c group By c.SERVICEINSTANCEID) P "
            + " ON a.SERVICEINSTANCEID = p.SERVICEINSTANCEID "
            + " left join siebel.s_order_item b "
            + " ON a.SERVICEINSTANCEID = b.ROOT_ORDER_ITEM_ID  and b.row_id = b.ROOT_ORDER_ITEM_ID "
            + " Left join "
            + " (SELECT  xm.par_row_id,  i.X_PAR_ROW_ID, i.X_PROVIDER, p.name Provider "
            + " FROM SIEBEL.CX_PROV_COMM_XM  xm, siebel.cx_prov_comment i, siebel.s_org_ful p "
            + " where i.ROW_ID = xm.PAR_ROW_ID and p.row_id= i.X_PROVIDER and i.X_PROVIDER_TYPE not in('Second')) h "
            + " ON a.SERVICEINSTANCEID = h.X_PAR_ROW_ID "
            + " WHERE a.PARENTACCOUNTNAME NOT LIKE 'Norwest%Venture%' "
            + " AND a.SERVICEINSTANCEID  IN(:sid) "
            + " Group by a.SERVICEINSTANCEID,  p.MRR, p.NRR,a.PARENTACCOUNTNAME, a.VLID, a.SERVICEACCOUNTNAME, a.X_NTT_W_NUMBER, "
            + " a.SERVICEINSTANCENAME, a.X_SERVICE_TYPE, a.X_TERM, b.CREATED, h.PROVIDER, b.X_RENEWAL_DT, b.X_RENEWAL_TERM, "
            + " b.X_RENEWAL_END_DATE, a.BILLINGSTARTDATE, a.BILLINGENDDATE "
            + " ORDER by a.SERVICEINSTANCEID", nativeQuery = true)
    public SidInfo findSidInfo(@Param("sid") String sid);

}
