package net.nttglobal.revdashws.seibelrepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import net.nttglobal.revdashws.seibelmodel.CustomerRollup;



@Repository
public interface CustomerRollupRepository extends JpaRepository<CustomerRollup, String> {

	@Query(value = " SELECT DISTINCT " 
			+" rl.ROLLUP " 
			+" FROM  vware4.x_siebel_orders_sid a " 
			+" LEFT JOIN "
			+" (Select distinct a.SERVICEINSTANCEID, nvl(c.name,d.name) ROLLUP "
			+" from siebel.x_siebel_orders a "
			+" LEFT JOIN siebel.S_ORG_EXT  b "
			+" ON a.PARENTACCOUNTID = b.ROW_ID "
			+" LEFT JOIN siebel.S_ORG_EXT c "
			+" ON c.row_id = b.FBI_FIELD_OFF_ID "
			+" LEFT JOIN siebel.s_org_ext d "
			+" on d.row_id = b.x_par_cust_id) rl " 
			+" ON a.SERVICEINSTANCEID = rl.SERVICEINSTANCEID " 
			+" WHERE a.PARENTACCOUNTNAME NOT LIKE '%CID%ACG%' " 
			+" AND a.BILLINGSTARTDATE is not null " 
			+" AND a.SERVICEINSTANCESTATUS NOT IN ('Cancelled') " 
			+" ORDER BY rl.ROLLUP ", nativeQuery=true)
	public List<CustomerRollup> findCustRollupBau();
	
	@Query(value = " SELECT DISTINCT " 
			+" rl.ROLLUP " 
			+" FROM vware4.x_siebel_orders_sid a " 
			+" LEFT JOIN "
			+" (Select distinct a.SERVICEINSTANCEID, nvl(c.name,d.name) ROLLUP "
			+" from siebel.x_siebel_orders a "
			+" LEFT JOIN siebel.S_ORG_EXT  b "
			+" ON a.PARENTACCOUNTID = b.ROW_ID "
			+" LEFT JOIN siebel.S_ORG_EXT  c "
			+" ON c.row_id = b.FBI_FIELD_OFF_ID "
			+" LEFT JOIN siebel.s_org_ext d "
			+" on d.row_id = b.x_par_cust_id) rl " 
			+" ON a.SERVICEINSTANCEID = rl.SERVICEINSTANCEID " 
			+" WHERE a.PARENTACCOUNTNAME LIKE '%CID%ACG%' " 
			+" AND a.BILLINGSTARTDATE is not null " 
			+" AND a.SERVICEINSTANCESTATUS NOT IN ('Cancelled') "
			+" AND rownum <= 10"
			+" ORDER BY rl.ROLLUP ", nativeQuery=true)
	public List<CustomerRollup> findCustRollupNtt();
	
}
