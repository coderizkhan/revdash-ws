package net.nttglobal.revdashws.helperclasses;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import net.nttglobal.revdashws.controller.CustomerContractController;

public class Uploads {
	
	@Value("${revenue.dashboard.contract.template.uploads}")
    private String uploadPath;

	public void uploadMultipleFiles(MultipartFile[] files) {
        
        for(MultipartFile file : files){
            Path fileNameAndPath = Paths.get(uploadPath, file.getOriginalFilename());
            try {
                Files.write(fileNameAndPath, file.getBytes());
            } catch (IOException ex) {
                Logger.getLogger(CustomerContractController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
	
	public void uploadSingleFile(MultipartFile file) {
		Path fileNameAndPath = Paths.get(uploadPath, file.getOriginalFilename());
		try {
			Files.write(fileNameAndPath, file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
