package net.nttglobal.revdashws.seibelcontroller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.nttglobal.revdashws.seibelmodel.ActiveMrr;
import net.nttglobal.revdashws.seibelmodel.SidInfo;
import net.nttglobal.revdashws.service.seibel.SeibelService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/seibel")
public class SeibelController {

	@Autowired
	private SeibelService service;

	//private SimpleDateFormat dateFormat = new SimpleDateFormat("mm-dd-yyyy");
	
	@GetMapping("/sidinfo/{sid}")
	public ResponseEntity<SidInfo> getSidInfo(@PathVariable String sid) throws ParseException{
		final SidInfo sidInfo = this.service.sidInfo(sid);
		String bsd = sidInfo.getBSD().substring(0, 10);
		String oed = sidInfo.getOED().substring(0, 10);
		sidInfo.setBSD(bsd);
		sidInfo.setOED(oed);
		return ResponseEntity.status(HttpStatus.OK).body(sidInfo);
	}
	
	@GetMapping("/activemrr")
	public ResponseEntity<List<ActiveMrr>> getActiveMrr(){
		final List<ActiveMrr> activeMrrList = this.service.activeMrr();
		return ResponseEntity.status(HttpStatus.OK).body(activeMrrList);
	}
	
}
