package net.nttglobal.revdashws.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author rizwan
 */
public class EtcValueUploadDto {

    private final Long id;
    private final String value;

    @JsonCreator
    public EtcValueUploadDto(@JsonProperty("id") Long id,
            @JsonProperty("value") String value) {
        this.id = id;
        this.value = value;
    }

    public EtcValueUploadDto(EtcValueUploadDto dto) {
        this.id = dto.getId();
        this.value = dto.getValue();
    }

    public Long getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

}
