package net.nttglobal.revdashws.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author rizwan
 */
public class MinimumMonthlyFeeParamDto {

    private float fee;
    private String period;
    

    @JsonCreator
    public MinimumMonthlyFeeParamDto(@JsonProperty("fee") float fee, @JsonProperty("period") String period) {
        this.fee = fee;
        this.period = period;
    }

    public MinimumMonthlyFeeParamDto() {

    }

    public float getFee() {
        return fee;
    }

    public void setFee(float fee) {
        this.fee = fee;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

}
