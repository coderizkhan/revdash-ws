package net.nttglobal.revdashws.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import net.nttglobal.revdashws.model.CustomerContract;

/**
 *
 * @author rizwan
 */
public class MinimumMonthlyFeeUploadDto {

    private final Long id;
    private final String period;
    private final float fee;

    @JsonCreator
    public MinimumMonthlyFeeUploadDto(@JsonProperty("id") Long id,
            @JsonProperty("period") String period,
            @JsonProperty("fee") float fee) {
        this.id = id;
        this.period = period;
        this.fee = fee;
    }

    public MinimumMonthlyFeeUploadDto(MinimumMonthlyFeeUploadDto dto) {
        this.id = dto.getId();
        this.period = dto.getPeriod();
        this.fee = dto.getFee();
    }

    public Long getId() {
        return id;
    }

    public String getPeriod() {
        return period;
    }

    public float getFee() {
        return fee;
    }


}
