package net.nttglobal.revdashws.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import java.sql.Timestamp;
import net.nttglobal.revdashws.model.CustomerContract;
import net.nttglobal.revdashws.model.EtlDates;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author rizwan
 */
public class EtlDataParamDto {

    @NotNull
    private final String sid;
    private final String contractCustName;
    private final Timestamp autoRenewalStartDate;
    private final Timestamp autoRenewalEndDate;
    private final Timestamp customerNotifiedDate;
    private final Timestamp billEndDate;
    private final Timestamp cwd;
    private final String specialCondition;
    private final double discount;
    private final float windDownFee;
    private final String reasonForDisconnect;
    private final String disconnectType;
    private final String customerGroup;
    private final float carrierEtl;
    private final String creatorRemarks;
    private final int noticePeriod;

    private final String orderTerm;
    private final String mrr;
    private final String nrr;
    private final String bsd;
    private final String oed;
    private final String renewalStartDate;
    private final String renewalTerm;
    private final String renewalEndDate;

    @JsonCreator
    public EtlDataParamDto(@JsonProperty("sid") String sid, @JsonProperty("contractCustName") String contractCustName,
                           @JsonProperty("autoRenewalStartDate") Timestamp autoRenewalStartDate,
                           @JsonProperty("autoRenewalEndDate") Timestamp autoRenewalEndDate,
                           @JsonProperty("customerNotifiedDate") Timestamp customerNotifiedDate,
                           @JsonProperty("billEndDate") Timestamp billEndDate, @JsonProperty("cwd") Timestamp cwd,
                           @JsonProperty("specialCondition") String specialCondition, @JsonProperty("discount") double discount,
                           @JsonProperty("windDownFee") float windDownFee,
                           @JsonProperty("reasonForDisconnect") String reasonForDisconnect,
                           @JsonProperty("disconnectType") String disconnectType, @JsonProperty("customerGroup") String customerGroup,
                           @JsonProperty("carrierEtl") float carrierEtl, @JsonProperty("creatorRemarks") String creatorRemarks,
                           @JsonProperty("noticePeriod") int noticePeriod, @JsonProperty("orderTerm") String orderTerm,
                           @JsonProperty("mrr") String mrr, @JsonProperty("nrr") String nrr, @JsonProperty("bsd") String bsd,
                           @JsonProperty("oed") String oed, @JsonProperty("renewalStartDate") String renewalStartDate,
                           @JsonProperty("renewalTerm") String renewalTerm, @JsonProperty("renewalEndDate") String renewalEndDate) {
        this.sid = sid;
        this.contractCustName = contractCustName;
        this.autoRenewalStartDate = autoRenewalStartDate;
        this.autoRenewalEndDate = autoRenewalEndDate;
        this.customerNotifiedDate = customerNotifiedDate;
        this.billEndDate = billEndDate;
        this.cwd = cwd;
        this.specialCondition = specialCondition;
        this.discount = discount;
        this.windDownFee = windDownFee;
        this.reasonForDisconnect = reasonForDisconnect;
        this.disconnectType = disconnectType;
        this.customerGroup = customerGroup;
        this.carrierEtl = carrierEtl;
        this.creatorRemarks = creatorRemarks;
        this.noticePeriod = noticePeriod;
        this.bsd = bsd;
        this.mrr = mrr;
        this.nrr = nrr;
        this.oed = oed;
        this.orderTerm = orderTerm;
        this.renewalEndDate = renewalEndDate;
        this.renewalStartDate = renewalStartDate;
        this.renewalTerm = renewalTerm;
    }

    public EtlDataParamDto(EtlDataParamDto paramDto) {
        this.sid = paramDto.sid;
        this.contractCustName = paramDto.contractCustName;
        this.autoRenewalStartDate = paramDto.autoRenewalStartDate;
        this.autoRenewalEndDate = paramDto.autoRenewalEndDate;
        this.customerNotifiedDate = paramDto.customerNotifiedDate;
        this.billEndDate = paramDto.billEndDate;
        this.cwd = paramDto.cwd;
        this.specialCondition = paramDto.specialCondition;
        this.discount = paramDto.discount;
        this.windDownFee = paramDto.windDownFee;
        this.reasonForDisconnect = paramDto.reasonForDisconnect;
        this.disconnectType = paramDto.disconnectType;
        this.customerGroup = paramDto.customerGroup;
        this.carrierEtl = paramDto.carrierEtl;
        this.creatorRemarks = paramDto.creatorRemarks;
        this.noticePeriod = paramDto.noticePeriod;
        this.bsd = paramDto.bsd;
        this.mrr = paramDto.mrr;
        this.nrr = paramDto.nrr;
        this.oed = paramDto.oed;
        this.orderTerm = paramDto.orderTerm;
        this.renewalEndDate = paramDto.renewalEndDate;
        this.renewalStartDate = paramDto.renewalStartDate;
        this.renewalTerm = paramDto.renewalTerm;
    }

    public EtlDataParamDto(EtlUserInfo userEtlInfo, CustomerContract customerContract, EtlDates finalDates) {
        this.sid = userEtlInfo.getSid();
        this.contractCustName = userEtlInfo.getContractCustName();
        this.autoRenewalStartDate = finalDates.getAutoRenewalStartDate();
        this.autoRenewalEndDate = finalDates.getAutoRenewalEndDate();
        this.customerNotifiedDate = userEtlInfo.getCustomerNotifiedDate();
        this.billEndDate = finalDates.getBillEndDate();
        this.cwd
                = userEtlInfo.getCwd();
        this.specialCondition
                = userEtlInfo.getSpecialCondition();
        this.discount = userEtlInfo.getDiscount();
        this.windDownFee = userEtlInfo.getWindDownFee();
        this.reasonForDisconnect
                = userEtlInfo.getReasonForDisconnect();
        this.disconnectType
                = userEtlInfo.getDisconnectType();
        this.customerGroup
                = userEtlInfo.getCustomerGroup();
        this.carrierEtl
                = userEtlInfo.getCarrierEtl();
        this.creatorRemarks
                = userEtlInfo.getCreatorRemarks();
        this.noticePeriod
                = customerContract.getErlyTerminationNoticeCust();
        this.bsd
                = userEtlInfo.getBsd();
        this.mrr = userEtlInfo.getMrr();
        this.nrr
                = userEtlInfo.getNrr();
        this.oed = userEtlInfo.getOed();
        this.orderTerm
                = userEtlInfo.getOrderTerm();
        this.renewalEndDate
                = userEtlInfo.getRenewalEndDate();
        this.renewalStartDate
                = userEtlInfo.getRenewalStartDate();
        this.renewalTerm
                = userEtlInfo.getRenewalTerm();
    }

    public String getSid() {
        return sid;
    }

    public String getContractCustName() {
        return contractCustName;
    }

    public Timestamp getAutoRenewalStartDate() {
        return autoRenewalStartDate;
    }

    public Timestamp getAutoRenewalEndDate() {
        return autoRenewalEndDate;
    }

    public Timestamp getCustomerNotifiedDate() {
        return customerNotifiedDate;
    }

    public Timestamp getBillEndDate() {
        return billEndDate;
    }

    public Timestamp getCwd() {
        return cwd;
    }

    public String getSpecialCondition() {
        return specialCondition;
    }

    public double getDiscount() {
        return discount;
    }

    public float getWindDownFee() {
        return windDownFee;
    }

    public String getReasonForDisconnect() {
        return reasonForDisconnect;
    }

    public String getDisconnectType() {
        return disconnectType;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public float getCarrierEtl() {
        return carrierEtl;
    }

    public String getCreatorRemarks() {
        return creatorRemarks;
    }

    public int getNoticePeriod() {
        return noticePeriod;
    }

    public String getOrderTerm() {
        return orderTerm;
    }

    public String getMrr() {
        return mrr;
    }

    public String getNrr() {
        return nrr;
    }

    public String getBsd() {
        return bsd;
    }

    public String getOed() {
        return oed;
    }

    public String getRenewalStartDate() {
        return renewalStartDate;
    }

    public String getRenewalTerm() {
        return renewalTerm;
    }

    public String getRenewalEndDate() {
        return renewalEndDate;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
