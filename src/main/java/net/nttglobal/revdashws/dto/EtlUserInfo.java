package net.nttglobal.revdashws.dto;

import java.sql.Timestamp;
import java.time.LocalDate;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

public class EtlUserInfo {

	@NotNull
	private final String sid;
	private final String contractCustName;
	private final Timestamp customerNotifiedDate;
	private final Timestamp cwd;
	private final String specialCondition;
	private final double discount;
	private final float windDownFee;
	private final String reasonForDisconnect;
	private final String disconnectType;
	private final String customerGroup;
	private final float carrierEtl;
	private final String creatorRemarks;

	private final String orderTerm;
	private final String mrr;
	private final String nrr;
	private final String bsd;
	private final String oed;
	private final String renewalStartDate;
	private final String renewalTerm;
	private final String renewalEndDate;
	
	@JsonCreator
	public EtlUserInfo(@JsonProperty("sid") String sid, @JsonProperty("contractCustName") String contractCustName,
			@JsonProperty("customerNotifiedDate") Timestamp customerNotifiedDate,
			@JsonProperty("billEndDate") Timestamp billEndDate, @JsonProperty("cwd") Timestamp cwd,
			@JsonProperty("specialCondition") String specialCondition, @JsonProperty("discount") double discount,
			@JsonProperty("windDownFee") float windDownFee,
			@JsonProperty("reasonForDisconnect") String reasonForDisconnect,
			@JsonProperty("disconnectType") String disconnectType, @JsonProperty("customerGroup") String customerGroup,
			@JsonProperty("carrierEtl") float carrierEtl, @JsonProperty("creatorRemarks") String creatorRemarks,
			@JsonProperty("orderTerm") String orderTerm, @JsonProperty("mrr") String mrr, @JsonProperty("nrr") String nrr, 
			@JsonProperty("bsd") String bsd, @JsonProperty("oed") String oed, @JsonProperty("renewalStartDate") String renewalStartDate,
			@JsonProperty("renewalTerm") String renewalTerm, @JsonProperty("renewalEndDate") String renewalEndDate) {
		this.sid = sid;
		this.contractCustName = contractCustName;
		this.customerNotifiedDate = customerNotifiedDate;
		this.cwd = cwd;
		this.specialCondition = specialCondition;
		this.discount = discount;
		this.windDownFee = windDownFee;
		this.reasonForDisconnect = reasonForDisconnect;
		this.disconnectType = disconnectType;
		this.customerGroup = customerGroup;
		this.carrierEtl = carrierEtl;
		this.creatorRemarks = creatorRemarks;
		this.bsd = bsd;
		this.mrr = mrr;
		this.nrr = nrr;
		this.oed = oed;
		this.orderTerm = orderTerm;
		this.renewalEndDate = renewalEndDate;
		this.renewalStartDate = renewalStartDate;
		this.renewalTerm = renewalTerm;
	}

	public String getSid() {
		return sid;
	}

	public String getContractCustName() {
		return contractCustName;
	}

	public Timestamp getCustomerNotifiedDate() {
		return customerNotifiedDate;
	}

	public Timestamp getCwd() {
		return cwd;
	}

	public String getSpecialCondition() {
		return specialCondition;
	}

	public double getDiscount() {
		return discount;
	}

	public float getWindDownFee() {
		return windDownFee;
	}

	public String getReasonForDisconnect() {
		return reasonForDisconnect;
	}

	public String getDisconnectType() {
		return disconnectType;
	}

	public String getCustomerGroup() {
		return customerGroup;
	}

	public float getCarrierEtl() {
		return carrierEtl;
	}

	public String getCreatorRemarks() {
		return creatorRemarks;
	}


	public String getOrderTerm() {
		return orderTerm;
	}

	public String getMrr() {
		return mrr;
	}

	public String getNrr() {
		return nrr;
	}

	public String getBsd() {
		return bsd;
	}

	public String getOed() {
		return oed;
	}

	public String getRenewalStartDate() {
		return renewalStartDate;
	}

	public String getRenewalTerm() {
		return renewalTerm;
	}

	public String getRenewalEndDate() {
		return renewalEndDate;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}
	
}
