package net.nttglobal.revdashws.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.Timestamp;
import net.nttglobal.revdashws.model.EtlData;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author rizwan
 */
public class EtlDataDto {

    private final String sid;
    private final String contractCustName;
    private final Timestamp autoRenewalStartDate;
    private final Timestamp autoRenewalEndDate;
    private final Timestamp customerNotifiedDate;
    private final Timestamp billEndDate;
    private final Timestamp cwd;
    private final String specialCondition;
    private final double discount;
    private final float windDownFee;
    private final String reasonForDisconnect;
    private final String disconnectType;
    private final String customerGroup;
    private final float carrierEtl;
    private final float customerEtl;
    private final String currentOwner;
    private final String creatorRemarks;
    private final int noticePeriod;

    @JsonCreator
    public EtlDataDto( @JsonProperty("sid") String sid, @JsonProperty("contractCustName") String contractCustName, 
    				@JsonProperty("autoRenewalStartDate") Timestamp autoRenewalStartDate, @JsonProperty("autoRenewalEndDate") Timestamp autoRenewalEndDate, 
    				@JsonProperty("customerNotifiedDate") Timestamp customerNotifiedDate, @JsonProperty("billEndDate") Timestamp billEndDate, 
    				@JsonProperty("cwd") Timestamp cwd, @JsonProperty("specialCondition") String specialCondition, @JsonProperty("discount") double discount, 
    				@JsonProperty("windDownFee") float windDownFee, @JsonProperty("reasonForDisconnect") String reasonForDisconnect, 
    				@JsonProperty("disconnectType") String disconnectType, @JsonProperty("customerGroup") String customerGroup, 
    				@JsonProperty("carrierEtl") float carrierEtl, @JsonProperty("customerEtl") float customerEtl, 
    				@JsonProperty("currentOwner") String currentOwner, @JsonProperty("creatorRemarks") String creatorRemarks,
    				@JsonProperty("noticePeriod") int noticePeriod) {
        this.sid = sid;
        this.contractCustName = contractCustName;
        this.autoRenewalStartDate = autoRenewalStartDate;
        this.autoRenewalEndDate = autoRenewalEndDate;
        this.customerNotifiedDate = customerNotifiedDate;
        this.billEndDate = billEndDate;
        this.cwd = cwd;
        this.specialCondition = specialCondition;
        this.discount = discount;
        this.windDownFee = windDownFee;
        this.reasonForDisconnect = reasonForDisconnect;
        this.disconnectType = disconnectType;
        this.customerGroup = customerGroup;
        this.carrierEtl = carrierEtl;
        this.customerEtl = customerEtl;
        this.currentOwner = currentOwner;
        this.creatorRemarks = creatorRemarks;
        this.noticePeriod = noticePeriod;
    }
    
    public EtlDataDto(EtlData etlData){
        this.sid = etlData.getSid();
        this.contractCustName = etlData.getContractCustName();
        this.autoRenewalStartDate = etlData.getAutoRenewalStartDate();
        this.autoRenewalEndDate = etlData.getAutoRenewalEndDate();
        this.customerNotifiedDate = etlData.getCustomerNotifiedDate();
        this.billEndDate = etlData.getBillEndDate();
        this.cwd = etlData.getCwd();
        this.specialCondition = etlData.getSpecialCondition();
        this.discount = etlData.getDiscount();
        this.windDownFee = etlData.getWindDownFee();
        this.reasonForDisconnect = etlData.getReasonForDisconnect();
        this.disconnectType = etlData.getDisconnectType();
        this.customerGroup = etlData.getCustomerGroup();
        this.carrierEtl = etlData.getCarrierEtl();
        this.customerEtl = etlData.getCustomerEtl();
        this.currentOwner = etlData.getCurrentOwner();
        this.creatorRemarks = etlData.getCreatorRemarks();
        this.noticePeriod = etlData.getNoticePeriod();
    }


    public String getSid() {
        return sid;
    }

    public String getContractCustName() {
        return contractCustName;
    }

    public Timestamp getAutoRenewalStartDate() {
        return autoRenewalStartDate;
    }

    public Timestamp getAutoRenewalEndDate() {
        return autoRenewalEndDate;
    }

    public Timestamp getCustomerNotifiedDate() {
        return customerNotifiedDate;
    }

    public Timestamp getBillEndDate() {
        return billEndDate;
    }

    public Timestamp getCwd() {
        return cwd;
    }

    public String getSpecialCondition() {
        return specialCondition;
    }

    public double getDiscount() {
        return discount;
    }

    public float getWindDownFee() {
        return windDownFee;
    }

    public String getReasonForDisconnect() {
        return reasonForDisconnect;
    }

    public String getDisconnectType() {
        return disconnectType;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public float getCarrierEtl() {
        return carrierEtl;
    }

    public float getCustomerEtl() {
        return customerEtl;
    }

    public String getCurrentOwner() {
        return currentOwner;
    }

    public String getCreatorRemarks() {
        return creatorRemarks;
    }

    public int getNoticePeriod() {
		return noticePeriod;
	}

	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
