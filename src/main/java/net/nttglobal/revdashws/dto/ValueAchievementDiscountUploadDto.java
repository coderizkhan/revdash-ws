package net.nttglobal.revdashws.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author rizwan
 */
public class ValueAchievementDiscountUploadDto {

    private final Long id;
    private final String slab;
    private final int billingPeriod;
    private final double percent;
    private final float amount;

    @JsonCreator
    public ValueAchievementDiscountUploadDto(@JsonProperty("id") Long id, 
            @JsonProperty("slab") String slab,
            @JsonProperty("billingPeriod") int billingPeriod,
            @JsonProperty("percent") double percent,
            @JsonProperty("amount") float amount) {
        this.id = id;
        this.slab = slab;
        this.billingPeriod = billingPeriod;
        this.percent = percent;
        this.amount = amount;
    }

    public ValueAchievementDiscountUploadDto(ValueAchievementDiscountUploadDto dto){
        this.id = dto.getId();
        this.slab = dto.getSlab();
        this.billingPeriod = dto.getBillingPeriod();
        this.percent = dto.getPercent();
        this.amount = dto.getAmount();
    }
    
    public Long getId() {
        return id;
    }

    public String getSlab() {
        return slab;
    }

    public int getBillingPeriod() {
        return billingPeriod;
    }

    public double getPercent() {
        return percent;
    }

    public float getAmount() {
        return amount;
    }

}
