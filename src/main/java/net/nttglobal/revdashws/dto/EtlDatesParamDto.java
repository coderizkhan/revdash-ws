package net.nttglobal.revdashws.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EtlDatesParamDto {

	private final String sid;
	private final Timestamp customerNotifiedDate;
	private final String customerGroup;
	private final String orderEndDate;
	private final String renewalEndDate;
	private final int noticePeriod;
	private final int autoRenewalServicePeriodTerm;

	@JsonCreator
	public EtlDatesParamDto(@JsonProperty("sid") String sid,
			@JsonProperty("customerNotifiedDate") Timestamp customerNotifiedDate,
			@JsonProperty("customerGroup") String customerGroup, @JsonProperty("orderEndDate") String orderEndDate,
			@JsonProperty("renewalEndDate") String renewalEndDate, @JsonProperty("noticePeriod") int noticePeriod,
			@JsonProperty("autoRenewalServicePeriodTerm") int autoRenewalServicePeriodTerm) {
		this.sid = sid;
		this.customerNotifiedDate = customerNotifiedDate;
		this.customerGroup = customerGroup;
		this.orderEndDate = orderEndDate;
		this.renewalEndDate = renewalEndDate;
		this.noticePeriod = noticePeriod;
		this.autoRenewalServicePeriodTerm = autoRenewalServicePeriodTerm;
	}


	public String getSid() {
		return sid;
	}

	public Timestamp getCustomerNotifiedDate() {
		return customerNotifiedDate;
	}

	public String getCustomerGroup() {
		return customerGroup;
	}

	public String getOrderEndDate() {
		return orderEndDate;
	}

	public String getRenewalEndDate() {
		return renewalEndDate;
	}

	public int getNoticePeriod() {
		return noticePeriod;
	}

	public int getAutoRenewalServicePeriodTerm() {
		return autoRenewalServicePeriodTerm;
	}

}
