package net.nttglobal.revdashws.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author rizwan
 */
public class CumulativeDiscountUploadDto {
    
    private final Long id;
    private final String period;
    private final float amount;
    private final int percent;
    
    @JsonCreator
    public CumulativeDiscountUploadDto(@JsonProperty("id") Long id, 
                                        @JsonProperty("period") String period, 
                                        @JsonProperty("amount") float amount, 
                                        @JsonProperty("percent") int percent){
        this.id = id;
        this.period = period;
        this.amount = amount;
        this.percent = percent;
    }
    
    public CumulativeDiscountUploadDto(CumulativeDiscountUploadDto dto){
        this.id = dto.getId();
        this.period = dto.getPeriod();
        this.amount = dto.getAmount();
        this.percent = dto.getPercent();
    }

    public Long getId() {
        return id;
    }

    public String getPeriod() {
        return period;
    }

    public float getAmount() {
        return amount;
    }

    public int getPercent() {
        return percent;
    }
    
    
    
}
