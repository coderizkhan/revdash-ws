package net.nttglobal.revdashws.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author rizwan
 */
public class CustomerContractUploadDto {

    private final String customerName;
    private final String businessUnit;
    private final String docType;
    private final Timestamp msaAmmendSignedDate;
    private final Timestamp msaAmmendCustEffDate;
    private final int termAgreementExtension;
    private final int minCommitPeriod;
    private final String minMthlyFee;
    private final int termRevCommit;
    private final int paymentTerm;
    private final float latePayChrg;
    private final int erlyTerminationNoticeCust;
    private final String spclTerm;
    private final String erlyTermPenaltyClause;
    private final String other_clause;
    private final String windDownFee;
    private final String windDownFeeCondition;
    private final String spclConditionETL;
    private final int initialSrvcPeriod;
    private final int srvcTermNewSrvc;
    private final String autoRenewServicePeriod;
    private final int srvcTermExistSrvc;
    private final float autoRenewServicePeriodTerm;
    private final int renewedExistService;
    private final int priceDiscEffectAfter;
    private final String discToCumu;
    private final String valAchvDisc;
    private final String renewalRemarks;
    private final String comments;
    private final String enteredBy;
    private final String fileName;
    private final String fileType;
    private final String fileSize;
    private final Timestamp enteredOn;
    private final Timestamp lastModified;
    private final String lastModifiedBy;

    @JsonCreator
    public CustomerContractUploadDto(@JsonProperty("customerName") String customerName,
            @JsonProperty("businessUnit") String businessUnit, @JsonProperty("docType") String docType,
            @JsonProperty("msaAmmendSignedDate") Timestamp msaAmmendSignedDate,
            @JsonProperty("msaAmmendCustEffDate") Timestamp msaAmmendCustEffDate,
            @JsonProperty("termAgreementExtension") int termAgreementExtension,
            @JsonProperty("minCommitPeriod") int minCommitPeriod, @JsonProperty("minMthlyFee") String minMthlyFee,
            @JsonProperty("termRevCommit") int termRevCommit, @JsonProperty("paymentTerm") int paymentTerm,
            @JsonProperty("latePayChrg") float latePayChrg,
            @JsonProperty("erlyTerminationNoticeCust") int erlyTerminationNoticeCust,
            @JsonProperty("spclTerm") String spclTerm,
            @JsonProperty("erlyTermPenaltyClause") String erlyTermPenaltyClause,
            @JsonProperty("other_clause") String other_clause, @JsonProperty("windDownFee") String windDownFee,
            @JsonProperty("windDownFeeCondition") String windDownFeeCondition,
            @JsonProperty("spclConditionETL") String spclConditionETL,
            @JsonProperty("initialSrvcPeriod") int initialSrvcPeriod,
            @JsonProperty("srvcTermNewSrvc") int srvcTermNewSrvc,
            @JsonProperty("srvcTermExistSrvc") int srvcTermExistSrvc,
            @JsonProperty("autoRenewServicePeriod") String autoRenewServicePeriod,
            @JsonProperty("autoRenewServicePeriodTerm") float autoRenewServicePeriodTerm,
            @JsonProperty("renewedExistService") int renewedExistService,
            @JsonProperty("priceDiscEffectAfter") int priceDiscEffectAfter,
            @JsonProperty("discToCumu") String discToCumu, @JsonProperty("valAchvDisc") String valAchvDisc,
            @JsonProperty("renewalRemarks") String renewalRemarks, @JsonProperty("comments") String comments,
            @JsonProperty("enteredBy") String enteredBy, @JsonProperty("fileName") String fileName,
            @JsonProperty("fileType") String fileType, @JsonProperty("fileSize") String fileSize,
            @JsonProperty("enteredOn") Timestamp enteredOn, @JsonProperty("lastModified") Timestamp lastModified,
            @JsonProperty("lastModifiedBy") String lastModifiedBy) {
        this.customerName = customerName;
        this.businessUnit = businessUnit;
        this.docType = docType;
        this.msaAmmendSignedDate = msaAmmendSignedDate;
        this.msaAmmendCustEffDate = msaAmmendCustEffDate;
        this.termAgreementExtension = termAgreementExtension;
        this.minCommitPeriod = minCommitPeriod;
        this.minMthlyFee = minMthlyFee;
        this.termRevCommit = termRevCommit;
        this.paymentTerm = paymentTerm;
        this.latePayChrg = latePayChrg;
        this.erlyTerminationNoticeCust = erlyTerminationNoticeCust;
        this.spclTerm = spclTerm;
        this.erlyTermPenaltyClause = erlyTermPenaltyClause;
        this.other_clause = other_clause;
        this.windDownFee = windDownFee;
        this.windDownFeeCondition = windDownFeeCondition;
        this.spclConditionETL = spclConditionETL;
        this.initialSrvcPeriod = initialSrvcPeriod;
        this.srvcTermNewSrvc = srvcTermNewSrvc;
        this.autoRenewServicePeriod = autoRenewServicePeriod;
        this.srvcTermExistSrvc = srvcTermExistSrvc;
        this.autoRenewServicePeriodTerm = autoRenewServicePeriodTerm;
        this.renewedExistService = renewedExistService;
        this.priceDiscEffectAfter = priceDiscEffectAfter;
        this.discToCumu = discToCumu;
        this.valAchvDisc = valAchvDisc;
        this.renewalRemarks = renewalRemarks;
        this.comments = comments;
        this.enteredBy = enteredBy;
        this.fileName = fileName;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.enteredOn = enteredOn;
        this.lastModified = lastModified;
        this.lastModifiedBy = lastModifiedBy;
    }

    public CustomerContractUploadDto(CustomerContractUploadDto dto) {
        this.customerName = dto.getCustomerName();
        this.businessUnit = dto.getBusinessUnit();
        this.docType = dto.getDocType();
        this.msaAmmendSignedDate = dto.getMsaAmmendSignedDate();
        this.msaAmmendCustEffDate = dto.getMsaAmmendCustEffDate();
        this.termAgreementExtension = dto.getTermAgreementExtension();
        this.minCommitPeriod = dto.getMinCommitPeriod();
        this.minMthlyFee = dto.getMinMthlyFee();
        this.termRevCommit = dto.getTermRevCommit();
        this.paymentTerm = dto.getPaymentTerm();
        this.latePayChrg = dto.getLatePayChrg();
        this.erlyTerminationNoticeCust = dto.getErlyTerminationNoticeCust();
        this.spclTerm = dto.getSpclTerm();
        this.erlyTermPenaltyClause = dto.getErlyTermPenaltyClause();
        this.other_clause = dto.getOther_clause();
        this.windDownFee = dto.getWindDownFee();
        this.windDownFeeCondition = dto.getWindDownFeeCondition();
        this.spclConditionETL = dto.getSpclConditionETL();
        this.initialSrvcPeriod = dto.getInitialSrvcPeriod();
        this.srvcTermNewSrvc = dto.getSrvcTermNewSrvc();
        this.autoRenewServicePeriod = dto.getAutoRenewServicePeriod();
        this.srvcTermExistSrvc = dto.getSrvcTermExistSrvc();
        this.autoRenewServicePeriodTerm = dto.getAutoRenewServicePeriodTerm();
        this.renewedExistService = dto.getRenewedExistService();
        this.priceDiscEffectAfter = dto.getPriceDiscEffectAfter();
        this.discToCumu = dto.getDiscToCumu();
        this.valAchvDisc = dto.getValAchvDisc();
        this.renewalRemarks = dto.getRenewalRemarks();
        this.comments = dto.getComments();
        this.enteredBy = dto.getEnteredBy();
        this.fileName = dto.getFileName();
        this.fileType = dto.getFileType();
        this.fileSize = dto.getFileSize();
        this.enteredOn = dto.getEnteredOn();
        this.lastModified = dto.getLastModified();
        this.lastModifiedBy = dto.getLastModifiedBy();
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public String getDocType() {
        return docType;
    }

    public Timestamp getMsaAmmendSignedDate() {
        return msaAmmendSignedDate;
    }

    public Timestamp getMsaAmmendCustEffDate() {
        return msaAmmendCustEffDate;
    }

    public int getTermAgreementExtension() {
        return termAgreementExtension;
    }

    public int getMinCommitPeriod() {
        return minCommitPeriod;
    }

    public String getMinMthlyFee() {
        return minMthlyFee;
    }

    public int getTermRevCommit() {
        return termRevCommit;
    }

    public int getPaymentTerm() {
        return paymentTerm;
    }

    public float getLatePayChrg() {
        return latePayChrg;
    }

    public int getErlyTerminationNoticeCust() {
        return erlyTerminationNoticeCust;
    }

    public String getSpclTerm() {
        return spclTerm;
    }

    public String getErlyTermPenaltyClause() {
        return erlyTermPenaltyClause;
    }

    public String getOther_clause() {
        return other_clause;
    }

    public String getWindDownFee() {
        return windDownFee;
    }

    public String getWindDownFeeCondition() {
        return windDownFeeCondition;
    }

    public String getSpclConditionETL() {
        return spclConditionETL;
    }

    public int getInitialSrvcPeriod() {
        return initialSrvcPeriod;
    }

    public int getSrvcTermNewSrvc() {
        return srvcTermNewSrvc;
    }

    public String getAutoRenewServicePeriod() {
        return autoRenewServicePeriod;
    }

    public int getSrvcTermExistSrvc() {
        return srvcTermExistSrvc;
    }

    public float getAutoRenewServicePeriodTerm() {
        return autoRenewServicePeriodTerm;
    }

    public int getRenewedExistService() {
        return renewedExistService;
    }

    public int getPriceDiscEffectAfter() {
        return priceDiscEffectAfter;
    }

    public String getDiscToCumu() {
        return discToCumu;
    }

    public String getValAchvDisc() {
        return valAchvDisc;
    }

    public String getRenewalRemarks() {
        return renewalRemarks;
    }

    public String getComments() {
        return comments;
    }

    public String getEnteredBy() {
        return enteredBy;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public String getFileSize() {
        return fileSize;
    }

    public Timestamp getEnteredOn() {
        return enteredOn;
    }

    public Timestamp getLastModified() {
        return lastModified;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }
    
    
    
}
