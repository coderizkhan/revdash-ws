package net.nttglobal.revdashws.dbconfig;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@PropertySources({@PropertySource("classpath:application.properties")})
@EnableJpaRepositories(
		entityManagerFactoryRef = "ds2EntityManager",
		transactionManagerRef = "ds2TransactionManager",
		basePackages = "net.nttglobal.revdashws.seibelrepository"
		)
public class SeibelDBConfig {

	@Autowired
	private Environment env;
	
	@Bean(name = "seibelDataSource")
	@ConfigurationProperties(prefix = "spring.seibel.datasource")
	public DataSource ds2DataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("spring.seibel.datasource.driver-class-name"));
		dataSource.setUrl(env.getProperty("spring.seibel.datasource.url"));
		dataSource.setUsername(env.getProperty("spring.seibel.datasource.username"));
		dataSource.setPassword(env.getProperty("spring.seibel.datasource.password"));
		return dataSource;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean ds2EntityManager() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(ds2DataSource());
		
		// scan entities in packages
		em.setPackagesToScan(new String[] {"net.nttglobal.revdashws.seibelmodel"});
		em.setPersistenceUnitName("seibel_database");
		
		// JPA & Hibernate
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		
		HashMap<String, Object> properties = new HashMap<>();
		
		// JPA & Hibernate
		properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect2"));
		properties.put("hibernate.show_sql", env.getProperty("spring.jpa.show-sql2"));
		//properties.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
		
		em.setJpaPropertyMap(properties);
		em.afterPropertiesSet();
		return em;
	}
	
	@Bean
	public PlatformTransactionManager ds2TransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(ds2EntityManager().getObject());
		return transactionManager;
	}
	
}
