package net.nttglobal.revdashws.dbconfig;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@PropertySources({@PropertySource("classpath:application.properties")})
@EnableJpaRepositories(
		entityManagerFactoryRef = "ds1EntityManager",
		transactionManagerRef = "ds1TransactionManager",
		basePackages = "net.nttglobal.revdashws.repository"
		)
public class RtmDbConfig {

	@Autowired
	private Environment env;
	
	@Primary
	@Bean(name = "rtmDataSource")
	@ConfigurationProperties(prefix = "spring.rtm.datasource")
	public DataSource ds1DataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("spring.rtm.datasource.driver-class-name"));
		dataSource.setUrl(env.getProperty("spring.rtm.datasource.url"));
		dataSource.setUsername(env.getProperty("spring.rtm.datasource.username"));
		dataSource.setPassword(env.getProperty("spring.rtm.datasource.password"));
		return dataSource;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean ds1EntityManager() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(ds1DataSource());
		
		// scan entities in packages
		em.setPackagesToScan(new String[] {"net.nttglobal.revdashws.model"});
		em.setPersistenceUnitName("rtm_database");
		
		// JPA & Hibernate
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		
		HashMap<String, Object> properties = new HashMap<>();
		
		// JPA & Hibernate
		properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
		properties.put("hibernate.show_sql", env.getProperty("spring.jpa.show-sql"));
		properties.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
		
		em.setJpaPropertyMap(properties);
		em.afterPropertiesSet();
		return em;
	}
	
	@Bean
	public PlatformTransactionManager ds1TransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(ds1EntityManager().getObject());
		return transactionManager;
	}
	
}
