package net.nttglobal.revdashws.constant;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Constants {

	private Constants() {
        // Not to be initialized
    }

    public static final List<String> BOOLEAN_TRUE_VALUES = Collections.unmodifiableList(Arrays.asList("1", "T", "Y", "YES", "TRUE"));

    public static final String TX_MGR_JPA_CONFIG = "configJpaTransactionManager";
    public static final String TX_MGR_JPA_SALES = "salesJpaTransactionManager";
    
    public static final String BAU = "BAU";
    public static final String NTT = "NTT";
    public static final String TWELVEMONTHMRR = "12 months MRR";
    public static final String REMAININGOFTWELVEMONTHMRR = "Remaining of 12 months MRR";
    public static final String NOETLPOSTTWELVEMONTH = "No ETL post 12 month";
    public static final String WINDDOWNFEES = "Wind Down Fees";
    public static final String KRAFT = "Kraft";
    public static final String MONDELEZ = "Mondelez";
    public static final String DISCOUNTEDETL = "Discounted ETL";
    public static final String TRANSITTOOTHERVENDOR = "Transit to another vendor";
    public static final String CLOSINGSITE = "closing site";
    public static final int ONEDAY = 24*60*60*1000;
    public static final int CONVERTTODAYS = 1000*60*60*24;
    public static final double CONVERTMONTHTODAY = 30.41666666666667;

    public static final String COLON = ":";
    public static final String COMMA = ",";
    public static final String DOT = ".";
    public static final String EMPTY_STRING = "";
    public static final String SEMI_COLON = ";";
    public static final String SLASH = "/";
    public static final String ASTERISK = "*";
    public static final String SPACE = " ";
    public static final String DASH = "-";
    public static final String NULL = "null";

    public static final String DATE_FORMAT_STD = "MM/dd/yyyy";
    public static final String DATE_FORMAT_LOG = "dd MMM uuuu";


    public static final String STATUS = "Status";
    
    public static final String DUMMYDATE = "1970-11-28 00:00:00";
	
}
