package net.nttglobal.revdashws.enumeration;

/**
 *
 * @author rizwan
 */
public enum EtlLov {

    OPEN,
    APPROVED,
    REJECTED,
    CLOSED;

}
