package net.nttglobal.revdashws.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author rizwan
 */
@Entity
@Table(name = "CUSTOMER_CONTRACT")
public class CustomerContract {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String customerName;
	private String businessUnit;
	private String docType;
	private Timestamp msaAmmendSignedDate;
	private Timestamp msaAmmendCustEffDate;
	private int termAgreementExtension;
	private int minCommitPeriod;
	private String minMthlyFee;
	private int termRevCommit;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "contract_id")
	private Set<MinimumMonthlyFee> minMnthlyFeeObj = new HashSet<>();
	private int paymentTerm;
	private float latePayChrg;
	private int erlyTerminationNoticeCust;
	@Column(columnDefinition = "text")
	private String spclTerm;
	private String erlyTermPenaltyClause;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "contract_id")
	private Set<EtcValue> etcValues = new HashSet<>();
	@Column(columnDefinition = "text")
	private String other_clause;
	private String windDownFee;
	@Column(columnDefinition = "text")
	private String windDownFeeCondition;
	@Column(columnDefinition = "text")
	private String spclConditionETL;
	private int initialSrvcPeriod;
	private int srvcTermNewSrvc;
	private String autoRenewServicePeriod;
	private int srvcTermExistSrvc;
	private int autoRenewServicePeriodTerm;
	private int renewedExistService;
	private int priceDiscEffectAfter;
	private String discToCumu;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "contract_id")
	private Set<CumulativeDiscount> cumulativeDiscount = new HashSet<>();
	private String valAchvDisc;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "contract_id")
	private Set<ValueAchievementDiscount> valAchDiscObj = new HashSet<>();
	@Column(columnDefinition = "text")
	private String renewalRemarks;
	@Column(columnDefinition = "text")
	private String comments;
	private String enteredBy;
	private String fileName;
	private String fileType;
	private Long fileSize;
	private LocalDateTime enteredOn;
	private LocalDateTime lastModified;
	private String lastModifiedBy;

	public CustomerContract() {
	}

	public CustomerContract(String customerName, String businessUnit, String docType, Timestamp msaAmmendSignedDate,
			Timestamp msaAmmendCustEffDate, int termAgreementExtension, int minCommitPeriod, String minMthlyFee,
			int termRevCommit, Set<MinimumMonthlyFee> minMnthlyFeeObj, int paymentTerm, float latePayChrg,
			int erlyTerminationNoticeCust, String spclTerm, String erlyTermPenaltyClause, Set<EtcValue> etcValues,
			String other_clause, String windDownFee, String windDownFeeCondition, String spclConditionETL,
			int initialSrvcPeriod, int srvcTermNewSrvc, String autoRenewServicePeriod, int srvcTermExistSrvc,
			int autoRenewServicePeriodTerm, int renewedExistService, int priceDiscEffectAfter, String discToCumu,
			Set<CumulativeDiscount> cumulativeDiscount, String valAchvDisc, Set<ValueAchievementDiscount> valAchDiscObj,
			String renewalRemarks, String comments, String enteredBy, String fileName, String fileType, Long fileSize,
			LocalDateTime enteredOn, LocalDateTime lastModified, String lastModifiedBy) {
		this.customerName = customerName;
		this.businessUnit = businessUnit;
		this.docType = docType;
		this.msaAmmendSignedDate = msaAmmendSignedDate;
		this.msaAmmendCustEffDate = msaAmmendCustEffDate;
		this.termAgreementExtension = termAgreementExtension;
		this.minCommitPeriod = minCommitPeriod;
		this.minMthlyFee = minMthlyFee;
		this.termRevCommit = termRevCommit;
		this.minMnthlyFeeObj = minMnthlyFeeObj;
		this.paymentTerm = paymentTerm;
		this.latePayChrg = latePayChrg;
		this.erlyTerminationNoticeCust = erlyTerminationNoticeCust;
		this.spclTerm = spclTerm;
		this.erlyTermPenaltyClause = erlyTermPenaltyClause;
		this.etcValues = etcValues;
		this.other_clause = other_clause;
		this.windDownFee = windDownFee;
		this.windDownFeeCondition = windDownFeeCondition;
		this.spclConditionETL = spclConditionETL;
		this.initialSrvcPeriod = initialSrvcPeriod;
		this.srvcTermNewSrvc = srvcTermNewSrvc;
		this.autoRenewServicePeriod = autoRenewServicePeriod;
		this.srvcTermExistSrvc = srvcTermExistSrvc;
		this.autoRenewServicePeriodTerm = autoRenewServicePeriodTerm;
		this.renewedExistService = renewedExistService;
		this.priceDiscEffectAfter = priceDiscEffectAfter;
		this.discToCumu = discToCumu;
		this.cumulativeDiscount = cumulativeDiscount;
		this.valAchvDisc = valAchvDisc;
		this.valAchDiscObj = valAchDiscObj;
		this.renewalRemarks = renewalRemarks;
		this.comments = comments;
		this.enteredBy = enteredBy;
		this.fileName = fileName;
		this.fileType = fileType;
		this.fileSize = fileSize;
		this.enteredOn = enteredOn;
		this.lastModified = lastModified;
		this.lastModifiedBy = lastModifiedBy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public Timestamp getMsaAmmendSignedDate() {
		return msaAmmendSignedDate;
	}

	public void setMsaAmmendSignedDate(Timestamp msaAmmendSignedDate) {
		this.msaAmmendSignedDate = msaAmmendSignedDate;
	}

	public Timestamp getMsaAmmendCustEffDate() {
		return msaAmmendCustEffDate;
	}

	public void setMsaAmmendCustEffDate(Timestamp msaAmmendCustEffDate) {
		this.msaAmmendCustEffDate = msaAmmendCustEffDate;
	}

	public int getTermAgreementExtension() {
		return termAgreementExtension;
	}

	public void setTermAgreementExtension(int termAgreementExtension) {
		this.termAgreementExtension = termAgreementExtension;
	}

	public int getMinCommitPeriod() {
		return minCommitPeriod;
	}

	public void setMinCommitPeriod(int minCommitPeriod) {
		this.minCommitPeriod = minCommitPeriod;
	}

	public String getMinMthlyFee() {
		return minMthlyFee;
	}

	public void setMinMthlyFee(String minMthlyFee) {
		this.minMthlyFee = minMthlyFee;
	}

	public int getTermRevCommit() {
		return termRevCommit;
	}

	public void setTermRevCommit(int termRevCommit) {
		this.termRevCommit = termRevCommit;
	}

	public Set<MinimumMonthlyFee> getMinMnthlyFeeObj() {
		return minMnthlyFeeObj;
	}

	public void setMinMnthlyFeeObj(Set<MinimumMonthlyFee> minMnthlyFeeObj) {
		this.minMnthlyFeeObj = minMnthlyFeeObj;
	}

	public int getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(int paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public float getLatePayChrg() {
		return latePayChrg;
	}

	public void setLatePayChrg(float latePayChrg) {
		this.latePayChrg = latePayChrg;
	}

	public int getErlyTerminationNoticeCust() {
		return erlyTerminationNoticeCust;
	}

	public void setErlyTerminationNoticeCust(int erlyTerminationNoticeCust) {
		this.erlyTerminationNoticeCust = erlyTerminationNoticeCust;
	}

	public String getSpclTerm() {
		return spclTerm;
	}

	public void setSpclTerm(String spclTerm) {
		this.spclTerm = spclTerm;
	}

	public String getErlyTermPenaltyClause() {
		return erlyTermPenaltyClause;
	}

	public void setErlyTermPenaltyClause(String erlyTermPenaltyClause) {
		this.erlyTermPenaltyClause = erlyTermPenaltyClause;
	}

	public Set<EtcValue> getEtcValues() {
		return etcValues;
	}

	public void setEtcValues(Set<EtcValue> etcValues) {
		this.etcValues = etcValues;
	}

	public String getOther_clause() {
		return other_clause;
	}

	public void setOther_clause(String other_clause) {
		this.other_clause = other_clause;
	}

	public String getWindDownFee() {
		return windDownFee;
	}

	public void setWindDownFee(String windDownFee) {
		this.windDownFee = windDownFee;
	}

	public String getWindDownFeeCondition() {
		return windDownFeeCondition;
	}

	public void setWindDownFeeCondition(String windDownFeeCondition) {
		this.windDownFeeCondition = windDownFeeCondition;
	}

	public String getSpclConditionETL() {
		return spclConditionETL;
	}

	public void setSpclConditionETL(String spclConditionETL) {
		this.spclConditionETL = spclConditionETL;
	}

	public int getInitialSrvcPeriod() {
		return initialSrvcPeriod;
	}

	public void setInitialSrvcPeriod(int initialSrvcPeriod) {
		this.initialSrvcPeriod = initialSrvcPeriod;
	}

	public int getSrvcTermNewSrvc() {
		return srvcTermNewSrvc;
	}

	public void setSrvcTermNewSrvc(int srvcTermNewSrvc) {
		this.srvcTermNewSrvc = srvcTermNewSrvc;
	}

	public String getAutoRenewServicePeriod() {
		return autoRenewServicePeriod;
	}

	public void setAutoRenewServicePeriod(String autoRenewServicePeriod) {
		this.autoRenewServicePeriod = autoRenewServicePeriod;
	}

	public int getSrvcTermExistSrvc() {
		return srvcTermExistSrvc;
	}

	public void setSrvcTermExistSrvc(int srvcTermExistSrvc) {
		this.srvcTermExistSrvc = srvcTermExistSrvc;
	}

	public int getAutoRenewServicePeriodTerm() {
		return autoRenewServicePeriodTerm;
	}

	public void setAutoRenewServicePeriodTerm(int autoRenewServicePeriodTerm) {
		this.autoRenewServicePeriodTerm = autoRenewServicePeriodTerm;
	}

	public int getRenewedExistService() {
		return renewedExistService;
	}

	public void setRenewedExistService(int renewedExistService) {
		this.renewedExistService = renewedExistService;
	}

	public int getPriceDiscEffectAfter() {
		return priceDiscEffectAfter;
	}

	public void setPriceDiscEffectAfter(int priceDiscEffectAfter) {
		this.priceDiscEffectAfter = priceDiscEffectAfter;
	}

	public String getDiscToCumu() {
		return discToCumu;
	}

	public void setDiscToCumu(String discToCumu) {
		this.discToCumu = discToCumu;
	}

	public Set<CumulativeDiscount> getCumulativeDiscount() {
		return cumulativeDiscount;
	}

	public void setCumulativeDiscount(Set<CumulativeDiscount> cumulativeDiscount) {
		this.cumulativeDiscount = cumulativeDiscount;
	}

	public String getValAchvDisc() {
		return valAchvDisc;
	}

	public void setValAchvDisc(String valAchvDisc) {
		this.valAchvDisc = valAchvDisc;
	}

	public Set<ValueAchievementDiscount> getValAchDiscObj() {
		return valAchDiscObj;
	}

	public void setValAchDiscObj(Set<ValueAchievementDiscount> valAchDiscObj) {
		this.valAchDiscObj = valAchDiscObj;
	}

	public String getRenewalRemarks() {
		return renewalRemarks;
	}

	public void setRenewalRemarks(String renewalRemarks) {
		this.renewalRemarks = renewalRemarks;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(String enteredBy) {
		this.enteredBy = enteredBy;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public LocalDateTime getEnteredOn() {
		return enteredOn;
	}

	public void setEnteredOn(LocalDateTime enteredOn) {
		this.enteredOn = enteredOn;
	}

	public LocalDateTime getLastModified() {
		return lastModified;
	}

	public void setLastModified(LocalDateTime lastModified) {
		this.lastModified = lastModified;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

}
