package net.nttglobal.revdashws.model;

import javax.persistence.*;
import net.nttglobal.revdashws.dto.ValueAchievementDiscountUploadDto;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author rizwan
 */
@Entity
@SequenceGenerator(name = "VAD_SEQ", sequenceName = "vad_seq")
public class ValueAchievementDiscount {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VAD_SEQ")
    private Long id;
    private String slab;
    private int billingPeriod;
    private double percent;
    private float amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSlab() {
        return slab;
    }

    public void setSlab(String slab) {
        this.slab = slab;
    }

    public int getBillingPeriod() {
        return billingPeriod;
    }

    public void setBillingPeriod(int billingPeriod) {
        this.billingPeriod = billingPeriod;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
