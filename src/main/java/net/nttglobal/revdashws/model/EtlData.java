package net.nttglobal.revdashws.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import net.nttglobal.revdashws.dto.EtlDataParamDto;
import net.nttglobal.revdashws.enumeration.EtlLov;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.NaturalId;

/**
 *
 * @author rizwan
 */
@Entity
@Table(name = "ETL_DATA")
public class EtlData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NaturalId
	private String etlCode;
	private String sid;
	private String contractCustName;
	private Timestamp autoRenewalStartDate;
	private Timestamp autoRenewalEndDate;
	private Timestamp customerNotifiedDate;
	private Timestamp billEndDate;
	private Timestamp cwd;
	private String specialCondition;
	private double discount;
	private float windDownFee;
	private String reasonForDisconnect;
	private String disconnectType;
	private String customerGroup;
	private float carrierEtl;
	private float customerEtl;
	private float approvedEtl;
	private String etlSid;
	@Enumerated(EnumType.STRING)
	private EtlLov etlStatus;
	private LocalDateTime enteredOn;
	private String enteredBy;
	private LocalDateTime lastModified;
	private String lastModifiedBy;
	private String currentOwner;
	@Column(columnDefinition = "text")
	private String creatorRemarks;
//	@OneToMany(mappedBy = "etlData", fetch = FetchType.EAGER) private
//	Set<ActionOnEtl> actions = new HashSet<>();
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "etlCode", referencedColumnName = "etlCode", nullable = false)
	private Set<FileModel> fileModel = new HashSet<>();
	private int noticePeriod;
        @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "etlCode", referencedColumnName = "etlCode", nullable = false, updatable = false, insertable = false)
	private Set<ActionOnEtl> actions = new HashSet<>();

	public EtlData() {

	}

	public EtlData(EtlDataParamDto etlData) {
		this.sid = etlData.getSid();
		this.contractCustName = etlData.getContractCustName();
		this.autoRenewalStartDate = etlData.getAutoRenewalStartDate();
		this.autoRenewalEndDate = etlData.getAutoRenewalEndDate();
		this.customerNotifiedDate = etlData.getCustomerNotifiedDate();
		this.billEndDate = etlData.getBillEndDate();
		this.cwd = etlData.getCwd();
		this.specialCondition = etlData.getSpecialCondition();
		this.discount = etlData.getDiscount();
		this.windDownFee = etlData.getWindDownFee();
		this.reasonForDisconnect = etlData.getReasonForDisconnect();
		this.disconnectType = etlData.getDisconnectType();
		this.customerGroup = etlData.getCustomerGroup();
		this.carrierEtl = etlData.getCarrierEtl();
		this.creatorRemarks = etlData.getCreatorRemarks();
		this.noticePeriod = etlData.getNoticePeriod();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEtlCode() {
		return etlCode;
	}

	public void setEtlCode(String etlCode) {
		this.etlCode = etlCode;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getContractCustName() {
		return contractCustName;
	}

	public void setContractCustName(String contractCustName) {
		this.contractCustName = contractCustName;
	}

	public Timestamp getAutoRenewalStartDate() {
		return autoRenewalStartDate;
	}

	public void setAutoRenewalStartDate(Timestamp autoRenewalStartDate) {
		this.autoRenewalStartDate = autoRenewalStartDate;
	}

	public Timestamp getAutoRenewalEndDate() {
		return autoRenewalEndDate;
	}

	public void setAutoRenewalEndDate(Timestamp autoRenewalEndDate) {
		this.autoRenewalEndDate = autoRenewalEndDate;
	}

	public Timestamp getCustomerNotifiedDate() {
		return customerNotifiedDate;
	}

	public void setCustomerNotifiedDate(Timestamp customerNotifiedDate) {
		this.customerNotifiedDate = customerNotifiedDate;
	}

	public Timestamp getBillEndDate() {
		return billEndDate;
	}

	public void setBillEndDate(Timestamp billEndDate) {
		this.billEndDate = billEndDate;
	}

	public Timestamp getCwd() {
		return cwd;
	}

	public void setCwd(Timestamp cwd) {
		this.cwd = cwd;
	}

	public String getSpecialCondition() {
		return specialCondition;
	}

	public void setSpecialCondition(String specialCondition) {
		this.specialCondition = specialCondition;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public float getWindDownFee() {
		return windDownFee;
	}

	public void setWindDownFee(float windDownFee) {
		this.windDownFee = windDownFee;
	}

	public String getReasonForDisconnect() {
		return reasonForDisconnect;
	}

	public void setReasonForDisconnect(String reasonForDisconnect) {
		this.reasonForDisconnect = reasonForDisconnect;
	}

	public String getDisconnectType() {
		return disconnectType;
	}

	public void setDisconnectType(String disconnectType) {
		this.disconnectType = disconnectType;
	}

	public String getCustomerGroup() {
		return customerGroup;
	}

	public void setCustomerGroup(String customerGroup) {
		this.customerGroup = customerGroup;
	}

	public float getCarrierEtl() {
		return carrierEtl;
	}

	public void setCarrierEtl(float carrierEtl) {
		this.carrierEtl = carrierEtl;
	}

	public float getCustomerEtl() {
		return customerEtl;
	}

	public void setCustomerEtl(float customerEtl) {
		this.customerEtl = customerEtl;
	}

	public float getApprovedEtl() {
		return approvedEtl;
	}

	public void setApprovedEtl(float approvedEtl) {
		this.approvedEtl = approvedEtl;
	}

	public String getEtlSid() {
		return etlSid;
	}

	public void setEtlSid(String etlSid) {
		this.etlSid = etlSid;
	}

	public EtlLov getEtlStatus() {
		return etlStatus;
	}

	public void setEtlStatus(EtlLov etlStatus) {
		this.etlStatus = etlStatus;
	}

	public LocalDateTime getEnteredOn() {
		return enteredOn;
	}

	public void setEnteredOn(LocalDateTime enteredOn) {
		this.enteredOn = enteredOn;
	}

	public String getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(String enteredBy) {
		this.enteredBy = enteredBy;
	}

	public LocalDateTime getLastModified() {
		return lastModified;
	}

	public void setLastModified(LocalDateTime lastModified) {
		this.lastModified = lastModified;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getCurrentOwner() {
		return currentOwner;
	}

	public void setCurrentOwner(String currentOwner) {
		this.currentOwner = currentOwner;
	}

	public String getCreatorRemarks() {
		return creatorRemarks;
	}

	public void setCreatorRemarks(String creatorRemarks) {
		this.creatorRemarks = creatorRemarks;
	}

	/*
	 * public Set<ActionOnEtl> getActions() { return actions; }
	 * 
	 * public void setActions(Set<ActionOnEtl> actions) { this.actions = actions; }
	 */

	public Set<FileModel> getFileModel() {
		return fileModel;
	}

	public void setFileModel(Set<FileModel> fileModel) {
		this.fileModel = fileModel;
	}
	

	public int getNoticePeriod() {
		return noticePeriod;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

    public Set<ActionOnEtl> getActions() {
        return actions;
    }

    public void setActions(Set<ActionOnEtl> actions) {
        this.actions = actions;
    }

}
