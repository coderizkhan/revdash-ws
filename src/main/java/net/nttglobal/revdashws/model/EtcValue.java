package net.nttglobal.revdashws.model;

import javax.persistence.*;
import net.nttglobal.revdashws.dto.EtcValueUploadDto;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author rizwan
 */
@Entity
@Table(name = "Etc_Value")
@SequenceGenerator(name = "ETCVALUE_SEQ", sequenceName = "etcvalue_seq")
public class EtcValue {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ETCVALUE_SEQ")
    private Long id;

    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
