package net.nttglobal.revdashws.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.Timestamp;

/**
 *
 * @author rizwan
 */
public class EtlDates {

	private Timestamp autoRenewalStartDate;
	private Timestamp autoRenewalEndDate;
	private Timestamp billEndDate;

	@JsonCreator
	public EtlDates(@JsonProperty("autoRenewalStartDate") Timestamp autoRenewalStartDate,
			@JsonProperty("autoRenewalEndDate") Timestamp autoRenewalEndDate,
			@JsonProperty("billEndDate") Timestamp billEndDate) {
		this.autoRenewalEndDate = autoRenewalEndDate;
		this.autoRenewalStartDate = autoRenewalStartDate;
		this.billEndDate = billEndDate;
	}

	public EtlDates() {

	}

	public Timestamp getAutoRenewalStartDate() {
		return autoRenewalStartDate;
	}

	public void setAutoRenewalStartDate(Timestamp autoRenewalStartDate) {
		this.autoRenewalStartDate = autoRenewalStartDate;
	}

	public Timestamp getAutoRenewalEndDate() {
		return autoRenewalEndDate;
	}

	public void setAutoRenewalEndDate(Timestamp autoRenewalEndDate) {
		this.autoRenewalEndDate = autoRenewalEndDate;
	}

	public Timestamp getBillEndDate() {
		return billEndDate;
	}

	public void setBillEndDate(Timestamp billEndDate) {
		this.billEndDate = billEndDate;
	}

}
