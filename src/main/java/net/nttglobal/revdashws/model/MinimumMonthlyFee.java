package net.nttglobal.revdashws.model;

import javax.persistence.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author rizwan
 */
@Entity
@Table(name = "Minimum_Monthly_Fee")
@SequenceGenerator(name = "MINMNTHLYFEE_SEQ", sequenceName = "minmnthlyfee_seq")
public class MinimumMonthlyFee {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MINMNTHLYFEE_SEQ")
    private Long id;
    private String period;
    private float fee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public float getFee() {
        return fee;
    }

    public void setFee(float fee) {
        this.fee = fee;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
