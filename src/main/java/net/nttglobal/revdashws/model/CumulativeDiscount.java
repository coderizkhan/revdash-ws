package net.nttglobal.revdashws.model;

import javax.persistence.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author rizwan
 */
@Entity
@Table(name = "Cumulative_Discount")
@SequenceGenerator(name = "CUMUDISC_SEQ", sequenceName = "cumudisc_seq")
public class CumulativeDiscount {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUMUDISC_SEQ")
    private Long id;
    private String period;
    private float amount;
    private int percent;

    public CumulativeDiscount() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
