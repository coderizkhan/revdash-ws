package net.nttglobal.revdashws.model;

import java.time.LocalDateTime;

import javax.persistence.*;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ActionOnEtl {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String action;
	@Column(columnDefinition = "text")
	private String remarks;
	private String actionBy;
	private LocalDateTime actionOn;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "etlCode", referencedColumnName = "etlCode", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private EtlData etlData;
	
	public ActionOnEtl() {
		
	}
	
	public ActionOnEtl(EtlData etl) {
		this.etlData = etl;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getActionBy() {
		return actionBy;
	}
	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}
	public LocalDateTime getActionOn() {
		return actionOn;
	}
	public void setActionOn(LocalDateTime actionOn) {
		this.actionOn = actionOn;
	}
	public EtlData getEtlData() {
		return etlData;
	}
	public void setEtlData(EtlData etlData) {
		this.etlData = etlData;
	}
	
	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
	
}
