package net.nttglobal.revdashws.utils;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.util.Strings;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseExcelParser implements AutoCloseable{

	private static final Logger LOG = LoggerFactory.getLogger(BaseExcelParser.class);

    private Workbook workBook;

    public static final String XLSX_FORMAT = "xlsx";

    public abstract int getStartRowIndex();

    public abstract String getSheetName();

    public BaseExcelParser(InputStream input) {
        super();
        try {
            this.workBook = new XSSFWorkbook(input);
        } catch (IOException ex) {
            LOG.info(ex.getMessage());
        }
    }

    /**
     * Identifies the cell data type and returns the data in the correct format
     *
     * @param cell
     *
     * @return
     */
    public Object getCellValue(Cell cell) {
        if (cell != null) {
            switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                return cell.getRichStringCellValue().getString().trim();
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                	Timestamp ts = new Timestamp(cell.getDateCellValue().getTime());
                	return ts;
                } else {
                    final BigDecimal val = BigDecimal.valueOf(cell.getNumericCellValue());
                    if (val.doubleValue() % 1 == 0) {
                        return Math.round(val.doubleValue());
                    }
                    return val;
                }
            case Cell.CELL_TYPE_BOOLEAN:
                return cell.getBooleanCellValue();
            case Cell.CELL_TYPE_FORMULA:
                return getCellFormulaValue(cell);
            default:
                return cell.getRichStringCellValue().getString().trim();
            }
        }
        return Strings.EMPTY;
    }

    private Object getCellFormulaValue(Cell cell) {
        final FormulaEvaluator evaluator = this.workBook.getCreationHelper().createFormulaEvaluator();
        final CellValue cellValue = evaluator.evaluate(cell);
        switch (cellValue.getCellType()) {
        case Cell.CELL_TYPE_BOOLEAN:
            return cellValue.getBooleanValue();
        case Cell.CELL_TYPE_NUMERIC:
            return BigDecimal.valueOf(cellValue.getNumberValue());
        default:
            return cellValue.getStringValue();
        }
    }

    public Sheet getSheetInstance() {

        if (this.workBook.getSheet(this.getSheetName()) == null) {
            final int firstSheetIndex = 0;
            return this.workBook.getSheetAt(firstSheetIndex);
        }
        return this.workBook.getSheet(this.getSheetName());
    }

    public List<List<Object>> extractRows(int hreaderIndex) {
        final List<List<Object>> objCollection = new ArrayList<>();
        final Sheet sheet = this.getSheetInstance();
        final int startIndex = this.getStartRowIndex();
        final int cellCount = sheet.getRow(hreaderIndex).getPhysicalNumberOfCells();
        int currentRowPos = 0;
        for (Row row : sheet) {
            if (currentRowPos >= startIndex) {
                final List<Object> objList = new ArrayList<>();
                for (int index = 0; index < cellCount; index++) {
                    objList.add(this.getCellValue(row.getCell(index)));
                }
                objCollection.add(objList);
            }
            currentRowPos += 1;
        }
        return objCollection;
    }

    public List<Object> extractRow(int rowIndex) {
        final List<Object> objectList = new ArrayList<>();
        final Sheet sheet = this.getSheetInstance();
        final int cellCount = sheet.getRow(rowIndex).getPhysicalNumberOfCells();
        final Row row = sheet.getRow(rowIndex);
        for (int cellIndex = 0; cellIndex < cellCount; cellIndex++) {
            objectList.add(this.getCellValue(row.getCell(cellIndex)));
        }
        return objectList;
    }

    @Override
    public void close() {
        if (this.workBook != null) {
            try {
                this.workBook.close();
            } catch (IOException e) {
                LOG.warn(e.getMessage());
            }
        }
    }
	
}
