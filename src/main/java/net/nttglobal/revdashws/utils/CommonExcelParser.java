package net.nttglobal.revdashws.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.nttglobal.revdashws.constant.Constants;

public class CommonExcelParser extends BaseExcelParser{

	private static final Logger LOG = LoggerFactory.getLogger(CommonExcelParser.class);

    private String sheetName;
    private int startRowIndex;
    private int headerIndex;

    public static class Builder {

        private InputStream input;

        private int startRowIndex = 0;
        private int headerIndex = 0;
        private String sheetName = null;

        public Builder(InputStream input) {
            this.input = input;
        }

        public Builder startRowIndex(int index) {
            this.startRowIndex = index;
            return this;
        }

        public Builder headerIndex(int index) {
            this.headerIndex = index;
            return this;
        }

        public Builder sheetName(String sheetName) {
            this.sheetName = sheetName;
            return this;
        }

        public CommonExcelParser build() {
            return new CommonExcelParser(this);
        }
    }

    public CommonExcelParser(Builder builder) {
        super(builder.input);
        this.headerIndex = builder.headerIndex;
        this.startRowIndex = builder.startRowIndex;
        this.sheetName = builder.sheetName;
    }

    @Override
    public int getStartRowIndex() {
        return startRowIndex;
    }

    @Override
    public String getSheetName() {
        if (this.sheetName == null) {
            this.sheetName = Strings.EMPTY;
        }
        return this.sheetName;
    }

    public List<Map<String, Object>> extractRowsHeader(Map<String, String> headerMap) {
        final List<Object> headerList = this.extractRow(this.headerIndex);
        final List<List<Object>> objectCollection = this.extractRows(this.headerIndex);
        final List<Map<String, Object>> objectMapList = new ArrayList<>();
        for (List<Object> objList : objectCollection) {
            final Map<String, Object> objMap = new HashMap<>();
            for (int index = 0; index < headerList.size(); index++) {
                final String header = headerMap.get(Objects.toString(headerList.get(index)));
                if (header != null) {
                    objMap.put(header, objList.get(index));
                }
            }
            objectMapList.add(objMap);
        }
        return objectMapList;
    }

    public String objectToJson(Object obj) {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            LOG.warn(e.getMessage());
        }
        return Constants.EMPTY_STRING;
    }

    
	public <T> T jasonToObject(String json, Class<?> type) {
        final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            return (T) mapper.readValue(json, type);
        } catch (IOException e) {
            LOG.warn(e.getMessage());
        }
        return (T) new Object();
    }

}
