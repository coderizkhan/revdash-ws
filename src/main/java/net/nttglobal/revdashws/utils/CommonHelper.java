package net.nttglobal.revdashws.utils;

import java.sql.Timestamp;
import java.util.Calendar;

public class CommonHelper {

	/**
     * set Timestamp time to 00:00:00
     *
     * @param timestamp
     * @return
     */
    public static Timestamp toExactDate(Timestamp timestamp) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(timestamp);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        final long exactTimStamp = cal.getTime().getTime();
        return new Timestamp(exactTimStamp);
    }
	
	/**
     * Get todays date with 00:00:00 time
     *
     * @param timestamp
     * @return
     */
    public static Timestamp getExactTimeStampToday() {
        return toExactDate(new Timestamp(System.currentTimeMillis()));
    }
}
