package net.nttglobal.revdashws.service;

import java.time.LocalDateTime;
import java.util.List;
import net.nttglobal.revdashws.exception.ResourceNotFoundException;
import net.nttglobal.revdashws.model.CustomerContract;
import net.nttglobal.revdashws.repository.CustomerContractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author rizwan
 */
@Service
@Transactional("ds1TransactionManager")
public class CustomerContractService {

	@Autowired
	private CustomerContractRepository custContRepo;

	public Long countCustomerContract() {
		return custContRepo.count();
	}

	public List<String> ListCustomerContract() {
		return this.custContRepo.CustomerContractList();
	}

	public CustomerContract addCustContract(CustomerContract contract, MultipartFile file) {
		//Uploads uploads = new Uploads();
		//uploads.uploadSingleFile(file);
		contract.setFileName(file.getOriginalFilename());
		contract.setFileSize(file.getSize());
		contract.setFileType(file.getContentType());
		contract.setEnteredOn(LocalDateTime.now());
		contract.setLastModified(LocalDateTime.now());
		return custContRepo.save(contract);
	}

	public List<CustomerContract> viewCustContract() {
		return custContRepo.findAll();
	}

	public CustomerContract getById(Long id) {
		return custContRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Contract Id", "id", id));
	}

	public CustomerContract getByCustName(String customerName) {
		return custContRepo.findByCustomerName(customerName);
	}

	public CustomerContract editContract(Long id, CustomerContract contract) {
		final CustomerContract custContract = this.getById(id);
		custContract.setFileName(contract.getFileName());
		custContract.setFileSize(contract.getFileSize());
		custContract.setFileType(contract.getFileType());
		custContract.setAutoRenewServicePeriod(contract.getAutoRenewServicePeriod());
		custContract.setAutoRenewServicePeriodTerm(contract.getAutoRenewServicePeriodTerm());
		custContract.setBusinessUnit(contract.getBusinessUnit());
		custContract.setComments(contract.getComments());
		custContract.setCustomerName(contract.getCustomerName());
		custContract.setDiscToCumu(contract.getDiscToCumu());
		custContract.setDocType(contract.getDocType());
		custContract.setErlyTerminationNoticeCust(contract.getErlyTerminationNoticeCust());
		custContract.setErlyTermPenaltyClause(contract.getErlyTermPenaltyClause());
		custContract.setInitialSrvcPeriod(contract.getInitialSrvcPeriod());
		custContract.setLastModifiedBy("rkhan");
		custContract.setLatePayChrg(contract.getLatePayChrg());
		custContract.setMinCommitPeriod(contract.getMinCommitPeriod());
		custContract.setMinMthlyFee(contract.getMinMthlyFee());
		custContract.setMsaAmmendCustEffDate(contract.getMsaAmmendCustEffDate());
		custContract.setMsaAmmendSignedDate(contract.getMsaAmmendSignedDate());
		custContract.setOther_clause(contract.getOther_clause());
		custContract.setPaymentTerm(contract.getPaymentTerm());
		custContract.setPriceDiscEffectAfter(contract.getPriceDiscEffectAfter());
		custContract.setRenewalRemarks(contract.getRenewalRemarks());
		custContract.setRenewedExistService(contract.getRenewedExistService());
		custContract.setSpclConditionETL(contract.getSpclConditionETL());
		custContract.setSpclTerm(contract.getSpclTerm());
		custContract.setSrvcTermExistSrvc(contract.getSrvcTermExistSrvc());
		custContract.setSrvcTermNewSrvc(contract.getSrvcTermNewSrvc());
		custContract.setTermAgreementExtension(contract.getTermAgreementExtension());
		custContract.setTermRevCommit(contract.getTermRevCommit());
		custContract.setWindDownFee(contract.getWindDownFee());
		custContract.setWindDownFeeCondition(contract.getWindDownFeeCondition());
		custContract.setEnteredOn(contract.getEnteredOn());
		custContract.setLastModified(LocalDateTime.now());
		custContract.setMinMnthlyFeeObj(contract.getMinMnthlyFeeObj());
		custContract.setCumulativeDiscount(contract.getCumulativeDiscount());
		custContract.setEtcValues(contract.getEtcValues());

		return this.custContRepo.save(custContract);
	}

	public void deleteCustContract(Long id) {
		this.custContRepo.deleteById(id);
	}

}
