package net.nttglobal.revdashws.service.seibel;

import java.util.List;

import net.nttglobal.revdashws.seibelmodel.ActiveMrr;
import net.nttglobal.revdashws.seibelmodel.BusinessUnit;
import net.nttglobal.revdashws.seibelmodel.CustomerRollup;
import net.nttglobal.revdashws.seibelmodel.SidInfo;
import net.nttglobal.revdashws.seibelmodel.TermStatus;
import net.nttglobal.revdashws.seibelrepository.ActiveMrrRepository;
import net.nttglobal.revdashws.seibelrepository.BusinessUnitRepository;
import net.nttglobal.revdashws.seibelrepository.CustomerRollupRepository;
import net.nttglobal.revdashws.seibelrepository.SidInfoRepository;
import net.nttglobal.revdashws.seibelrepository.TermStatusRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional("ds2TransactionManager")
public class SeibelService {

	@Autowired
	private ActiveMrrRepository activeMrrRepository;
	
	@Autowired
	private TermStatusRepository termStatusRepository;
	
	@Autowired
	private BusinessUnitRepository businessUnitRepository;
	
	@Autowired
	private CustomerRollupRepository custRollupRepository;
	
	@Autowired
	private SidInfoRepository sidInfoRepository;
	
	public List<ActiveMrr> activeMrr(){
		return activeMrrRepository.findActiveMrr();
	}
	
	public List<TermStatus> termStatusBau(){
		return termStatusRepository.findTermStatusBau();
	}
	
	public List<TermStatus> termStatusNtt(){
		return termStatusRepository.findTermStatusNtt();
	}
	
	public List<BusinessUnit> businessUnitBau(){
		return businessUnitRepository.findBusinessUnitBau();
	}
	
	public List<BusinessUnit> businessUnitNtt(){
		return businessUnitRepository.findBusinessUnitNtt();
	}
	
	public List<CustomerRollup> customerRollupBau(){
		return custRollupRepository.findCustRollupBau();
	}
	
	public List<CustomerRollup> customerRollupNtt(){
		return custRollupRepository.findCustRollupNtt();
	}
	
	public SidInfo sidInfo(String sid) {
		return sidInfoRepository.findSidInfo(sid);
	}
	
}
