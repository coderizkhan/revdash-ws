package net.nttglobal.revdashws.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import net.nttglobal.revdashws.constant.Constants;
import net.nttglobal.revdashws.dto.EtlDataDto;
import net.nttglobal.revdashws.dto.EtlDataParamDto;
import net.nttglobal.revdashws.dto.EtlUserInfo;
import net.nttglobal.revdashws.enumeration.EtlLov;
import net.nttglobal.revdashws.model.CustomerContract;
import net.nttglobal.revdashws.model.EtlData;
import net.nttglobal.revdashws.model.EtlDates;
import net.nttglobal.revdashws.model.FileModel;
import net.nttglobal.revdashws.repository.EtlDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author rizwan
 */
@Service
@Transactional("ds1TransactionManager")
public class EtlDataService {

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    //private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd 00:00:00");

    private static final AtomicLong BASETIME = new AtomicLong();
    private static final ReentrantLock TIMELOCK = new ReentrantLock();

    @Autowired
    private EtlDataRepository repo;

    @Autowired
    private CustomerContractService custContServe;

    public EtlDataDto etlCalculatedData(EtlUserInfo userEtlInfo) throws
            ParseException {
        final EtlDates finalDates = this.calcEtlDates(userEtlInfo);
        final CustomerContract customerData
                = this.custContServe.getByCustName(userEtlInfo.getContractCustName());
        final EtlDataParamDto etlDataParamDto = new EtlDataParamDto(userEtlInfo,
                                                                    customerData, finalDates);
        final Float etlAmount
                = this.calculateEtl(etlDataParamDto);
        final EtlData etlData = new EtlData(etlDataParamDto);
        etlData.setCustomerEtl(etlAmount);
        EtlDataDto etlDataDto = new EtlDataDto(etlData);

        return etlDataDto;
    }

    public List<EtlDataDto> addEtlData(List<EtlDataParamDto> dto, MultipartFile[] files) throws ParseException {

        Set<FileModel> fileModelList = new HashSet<>();
        for (MultipartFile file : files) {
            FileModel fileModel = new FileModel();
            fileModel.setFilename(file.getOriginalFilename());
            fileModelList.add(fileModel);
        }
        List<EtlData> etlDataList = new ArrayList<>();
        final String etlCode = this.generateEtlCode();

        for (EtlDataParamDto paramDto : dto) {
            EtlData etlData = new EtlData(paramDto);
            etlData.setEtlCode(etlCode);
            etlData.setCurrentOwner("jshah");
            etlData.setEnteredBy("rkhan");
            etlData.setEnteredOn(LocalDateTime.now());
            etlData.setEtlStatus(EtlLov.OPEN);
            etlData.setLastModified(LocalDateTime.now());
            etlData.setLastModifiedBy("rkhan");
            etlData.setCustomerEtl(Math.round(this.calculateEtl(paramDto)));
            etlData.setFileModel(fileModelList);
            etlDataList.add(etlData);
        }

        this.repo.saveAll(etlDataList);
        //Uploads uploads = new Uploads();
        //uploads.uploadMultipleFiles(files);
        List<EtlDataDto> etlDataDto = etlDataList.stream().map(map -> new EtlDataDto(map)).collect(Collectors.toList());

        return etlDataDto;
    }

    public EtlData updateEtlBySid(String sid, EtlData input) {
        EtlData etlData = this.getEtlDataBySid(sid);

        etlData.setApprovedEtl(input.getApprovedEtl());
        etlData.setAutoRenewalEndDate(input.getAutoRenewalEndDate());
        etlData.setAutoRenewalStartDate(input.getAutoRenewalStartDate());
        etlData.setBillEndDate(input.getBillEndDate());
        etlData.setCarrierEtl(input.getCarrierEtl());
        etlData.setCreatorRemarks(input.getCreatorRemarks());
        etlData.setCurrentOwner(input.getCurrentOwner());
        etlData.setCustomerEtl(input.getCustomerEtl());
        etlData.setCustomerNotifiedDate(input.getCustomerNotifiedDate());
        etlData.setCwd(input.getCwd());
        etlData.setDisconnectType(input.getDisconnectType());
        etlData.setDiscount(input.getDiscount());
        etlData.setLastModified(LocalDateTime.now());
        etlData.setLastModifiedBy(input.getLastModifiedBy());
        etlData.setReasonForDisconnect(input.getReasonForDisconnect());
        etlData.setSpecialCondition(input.getSpecialCondition());
        etlData.setWindDownFee(input.getWindDownFee());

        return this.repo.save(etlData);
    }

    public EtlData getEtlDataBySid(String sid) {
        return this.repo.findBySid(sid);
    }

    public EtlDates calcEtlDates(EtlUserInfo dto) throws ParseException {
        return this.getDatesForEtlCalc(dto);
    }

    public Float calculateCustomerEtl(EtlDataParamDto paramDto) throws ParseException {
        return this.calculateEtl(paramDto);
    }

    public List<EtlData> viewByEtlCode(String etlCode) {
        return this.repo.findByEtlCode(etlCode);
    }

    public void delete(Long id) {
        this.repo.deleteById(id);
    }

    public void updateEtlStatus(String etlCode, String status) {
        List<EtlData> finalList = new ArrayList<>();
        List<EtlData> etlDataList = this.repo.findByEtlCode(etlCode);
        for (EtlData etlData : etlDataList) {
            etlData.setEtlStatus(EtlLov.valueOf(status));
            etlData.setLastModified(LocalDateTime.now());
            etlData.setLastModifiedBy("username");
            finalList.add(etlData);
        }
        this.repo.saveAll(finalList);
    }

    public float calculateEtl(EtlDataParamDto paramDto) throws ParseException {
        float customerEtl = 0;
        Date dummyDate = dateFormat.parse(Constants.DUMMYDATE);

        //final SidInfo sidInfo = seibelService.sidInfo(paramDto.getSid());
        //final CustomerContract customerContract = this.customerContractService.getByCustName(paramDto.getContractCustName());
        Date _bsd = dateFormat.parse(paramDto.getBsd());
        Date _initialTermEndDate = dateFormat.parse(paramDto.getOed());
        Date _renewalStartDate = null;
        Date _renewalEndDate = null;
        if (paramDto.getRenewalStartDate() != null) {
            _renewalStartDate = dateFormat.parse(paramDto.getRenewalStartDate());
        }
        if (paramDto.getRenewalEndDate() != null) {
            _renewalEndDate = dateFormat.parse(paramDto.getRenewalEndDate());
        }
        int term = 0;
        int renewalTerm = 0;
        int diffInDays = 0;
        int remainDaysForFirstYr = 0;
        int remainDaysForSecondYr = 0;
        int remainDaysForThirdYr = 0;
        int initialTerm = Integer.parseInt(paramDto.getOrderTerm());
        if (paramDto.getRenewalTerm() != null) {
            renewalTerm = Integer.parseInt(paramDto.getRenewalTerm());
        }
        int noticePeriod = paramDto.getNoticePeriod();

        float mrr = Float.parseFloat(paramDto.getMrr());

        Timestamp bsd = new Timestamp(_bsd.getTime());
        Timestamp initialTermEndDate = new Timestamp(_initialTermEndDate.getTime());
        Timestamp renewalStartDate;
        Timestamp renewalEndDate;
        Timestamp autoRenewalStartDate = paramDto.getAutoRenewalStartDate();
        Timestamp autoRenewalEndDate = paramDto.getAutoRenewalEndDate();
        Timestamp bed = paramDto.getBillEndDate();
        Timestamp startDate;
        Timestamp dateForEtlCalc;
        Timestamp dateForMondelez;

        if (paramDto.getRenewalTerm() == null) {
            term = initialTerm;
        } else {
            term = renewalTerm;
        }

        if (paramDto.getRenewalStartDate() == null) {
            renewalStartDate = new Timestamp(dummyDate.getTime());
            renewalEndDate = new Timestamp(dummyDate.getTime());
        } else {
            renewalStartDate = new Timestamp(_renewalStartDate.getTime());
            renewalEndDate = new Timestamp(_renewalEndDate.getTime());
        }

        if (paramDto.getSpecialCondition() != null) {
            if (paramDto.getCustomerGroup().equals(Constants.BAU)) {
                if (initialTermEndDate.compareTo(renewalEndDate) > 0) {
                    startDate = initialTermEndDate;
                    if (this.addDays(startDate, -noticePeriod).compareTo(paramDto.getCustomerNotifiedDate()) < 0) {
                        dateForEtlCalc = autoRenewalStartDate;
                    } else {
                        if (bsd.compareTo(renewalStartDate) > 0) {
                            dateForEtlCalc = bsd;
                        } else {
                            dateForEtlCalc = renewalStartDate;
                        }
                    }
                } else {
                    startDate = renewalEndDate;
                    if (startDate.compareTo(paramDto.getCustomerNotifiedDate()) < 0) {
                        dateForEtlCalc = autoRenewalStartDate;
                    } else {
                        if (bsd.compareTo(renewalStartDate) > 0) {
                            dateForEtlCalc = bsd;
                        } else {
                            dateForEtlCalc = renewalStartDate;
                        }
                    }
                }

                switch (paramDto.getSpecialCondition()) {
                case Constants.TWELVEMONTHMRR:
                    diffInDays = 365;
                    break;
                case Constants.REMAININGOFTWELVEMONTHMRR:
                    if (dateForEtlCalc == autoRenewalStartDate) {
                        diffInDays = (int) (((this.addDays(autoRenewalStartDate, 365).getTime() - (paramDto.getBillEndDate().getTime())) / Constants.CONVERTTODAYS) / Constants.ONEDAY);
                    } else if (dateForEtlCalc.compareTo(bsd) == 0) {
                        diffInDays = (int) (((this.addDays(bsd, 365).getTime() - (paramDto.getBillEndDate().getTime())) / Constants.CONVERTTODAYS) / Constants.ONEDAY);
                    } else {
                        diffInDays = (int) (((this.addDays(renewalStartDate, 365).getTime() - (paramDto.getBillEndDate().getTime())) / Constants.CONVERTTODAYS) / Constants.ONEDAY);
                    }
                    break;
                case Constants.NOETLPOSTTWELVEMONTH:
                    if (dateForEtlCalc.compareTo(bsd) == 0) {
                        if (paramDto.getBillEndDate().compareTo(this.addDays(bsd, 365)) > 0) {
                            diffInDays = 0;
                        } else {
                            diffInDays = (int) ((this.addDays(bsd, 365).getTime() - (paramDto.getBillEndDate().getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY);
                        }
                    } else {
                        diffInDays = 0;
                    }
                    break;
                case Constants.WINDDOWNFEES:
                    diffInDays = (int) (paramDto.getWindDownFee() * Constants.CONVERTMONTHTODAY);
                    break;
                case Constants.KRAFT:
                    if (!paramDto.getReasonForDisconnect().isEmpty()) {
                        if (paramDto.getReasonForDisconnect().equals(Constants.CLOSINGSITE)) {
                            if (dateForEtlCalc.compareTo(bsd) == 0) {
                                if (paramDto.getBillEndDate().compareTo(initialTermEndDate) > 0) {
                                    diffInDays = 0;
                                } else if ((int) (((paramDto.getBillEndDate().getTime() - bsd.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) < 365) {
                                    diffInDays = 12;
                                } else if ((int) (((paramDto.getBillEndDate().getTime() - bsd.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) > 365
                                        && (int) (((paramDto.getBillEndDate().getTime() - bsd.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) < 730) {
                                    diffInDays = 9;
                                } else {
                                    diffInDays = 3;
                                }
                            } else {
                                if (paramDto.getBillEndDate().compareTo(initialTermEndDate) > 0) {
                                    diffInDays = 0;
                                } else if ((int) (((paramDto.getBillEndDate().getTime() - renewalStartDate.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) < 365) {
                                    diffInDays = 12;
                                } else if ((int) (((paramDto.getBillEndDate().getTime() - renewalStartDate.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) > 365
                                        && (int) (((paramDto.getBillEndDate().getTime() - renewalStartDate.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) < 730) {
                                    diffInDays = 9;
                                } else {
                                    diffInDays = 3;
                                }
                            }
                        }
                        if (paramDto.getReasonForDisconnect().equals(Constants.TRANSITTOOTHERVENDOR)) {
                            if (dateForEtlCalc.compareTo(bsd) == 0) {
                                if (paramDto.getBillEndDate().compareTo(initialTermEndDate) > 0) {
                                    diffInDays = 0;
                                } else if ((int) (((paramDto.getBillEndDate().getTime() - bsd.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) < 365) {
                                    diffInDays = 18;
                                } else if ((int) (((paramDto.getBillEndDate().getTime() - bsd.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) > 365
                                        && (int) (((paramDto.getBillEndDate().getTime() - bsd.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) < 730) {
                                    diffInDays = 12;
                                } else {
                                    diffInDays = 9;
                                }
                            } else if (dateForEtlCalc.compareTo(renewalStartDate) == 0) {
                                if (paramDto.getBillEndDate().compareTo(renewalEndDate) > 0) {
                                    diffInDays = 0;
                                } else if ((int) (((paramDto.getBillEndDate().getTime() - renewalStartDate.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) < 365) {
                                    diffInDays = 18;
                                } else if ((int) (((paramDto.getBillEndDate().getTime() - renewalStartDate.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) > 365
                                        && (int) (((paramDto.getBillEndDate().getTime() - renewalStartDate.getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) < 730) {
                                    diffInDays = 12;
                                } else {
                                    diffInDays = 9;
                                }
                            } else {
                                diffInDays = 0;
                            }
                        }
                    } else {
                        System.out.println("Select Reason for disconnect");
                    }
                    break;
                case Constants.MONDELEZ:
                    if (renewalStartDate.compareTo(bsd) > 0) {
                        dateForMondelez = renewalStartDate;
                    } else {
                        dateForMondelez = bsd;
                    }

                    remainDaysForFirstYr = (int) ((((this.addDays(dateForMondelez, 365)).getTime()
                                                    - paramDto.getBillEndDate().getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY);
                    if (remainDaysForFirstYr < 0) {
                        remainDaysForFirstYr = 0;
                    }

                    remainDaysForSecondYr = (int) ((((this.addDays(dateForMondelez, 365)).getTime()
                                                     - paramDto.getBillEndDate().getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) - remainDaysForFirstYr;
                    if (remainDaysForSecondYr < 0) {
                        remainDaysForSecondYr = 0;
                    }

                    remainDaysForThirdYr = (int) ((((this.addDays(dateForMondelez, 365)).getTime()
                                                    - paramDto.getBillEndDate().getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY) - (remainDaysForFirstYr + remainDaysForSecondYr);
                    if (remainDaysForThirdYr < 0) {
                        remainDaysForThirdYr = 0;
                    }
                    diffInDays = 0;

                    break;
                default:
                    if (dateForEtlCalc.compareTo(autoRenewalStartDate) == 0) {
                        diffInDays = (int) (((autoRenewalEndDate.getTime() - paramDto.getBillEndDate().getTime()) / Constants.CONVERTTODAYS));
                    } else if (dateForEtlCalc.compareTo(bsd) == 0) {
                        diffInDays = (int) (((initialTermEndDate.getTime() - bed.getTime()) / Constants.CONVERTTODAYS));
                    } else if (dateForEtlCalc.compareTo(renewalStartDate) == 0) {
                        diffInDays = (int) (((new Timestamp(_renewalEndDate.getTime()).getTime() - bed.getTime()) / Constants.CONVERTTODAYS));
                    }
                    break;
                }

                if (diffInDays > 0) {
                    if (paramDto.getSpecialCondition().equals(Constants.DISCOUNTEDETL)) {
                        customerEtl = (float) (diffInDays * (mrr / Constants.CONVERTMONTHTODAY) * (paramDto.getDiscount() / 100));
                    } else if (paramDto.getSpecialCondition().equals(Constants.KRAFT)) {
                        customerEtl = diffInDays * mrr;
                    } else {
                        customerEtl = (float) (diffInDays * (mrr / Constants.CONVERTMONTHTODAY));
                        System.out.println(customerEtl);
                    }
                } else {
                    if (paramDto.getSpecialCondition().equals(Constants.MONDELEZ)) {
                        float etlFirstYr = (float) (remainDaysForFirstYr * (mrr / Constants.CONVERTMONTHTODAY));
                        float etlSecondYr = (float) (remainDaysForSecondYr * (mrr / Constants.CONVERTMONTHTODAY)) * (75 / 100);
                        float etlThirdYr = (float) (remainDaysForSecondYr * (mrr / Constants.CONVERTMONTHTODAY)) * (50 / 100);

                        if (term == 12) {
                            customerEtl = etlFirstYr;
                        } else if (term == 24) {
                            customerEtl = etlFirstYr + etlSecondYr;
                        } else {
                            customerEtl = etlFirstYr + etlSecondYr + etlThirdYr;
                        }
                    } else {
                        customerEtl = 0;
                    }
                }
            } else {
                dateForEtlCalc = initialTermEndDate;
                diffInDays = (int) (((dateForEtlCalc.getTime() - paramDto.getBillEndDate().getTime()) / Constants.CONVERTTODAYS) / Constants.ONEDAY);
                if (diffInDays > 0) {
                    customerEtl = (float) (diffInDays * (mrr / Constants.CONVERTMONTHTODAY));
                } else {
                    customerEtl = 0;
                }
            }
        } else {
            System.out.println("Special condition cannot be empty");
        }
        return customerEtl;
    }

    public EtlDates getDatesForEtlCalc(EtlUserInfo dto) throws ParseException {
        final EtlDates etlDates = new EtlDates();
        final String customerName = dto.getContractCustName();
        CustomerContract contractData = this.custContServe.getByCustName(customerName);
        int noticePeriod = contractData.getErlyTerminationNoticeCust();
        System.out.println("Notice Perion: " + noticePeriod);
        int autoRenewalServicePeriodTerm = contractData.getAutoRenewServicePeriodTerm();
        System.out.println("Auto Renewal Service Period Term: " + autoRenewalServicePeriodTerm);
        int num = 0;
        Timestamp myDate;
        Date oed = dateFormat.parse(dto.getOed());
        Date renEndDt;
        Timestamp initialTermEndDate = new Timestamp(oed.getTime());
        Timestamp renewalEndDate;
        Timestamp autoRenewalEndDate;
        Timestamp autoRenewalStartDate;
        if (dto.getRenewalEndDate() != null) {
            renEndDt = dateFormat.parse(dto.getRenewalEndDate());
            System.out.println("renEndDt: " + renEndDt);
            renewalEndDate = new Timestamp(renEndDt.getTime());
            System.out.println("renewalEndDate: " + renewalEndDate);
            num = initialTermEndDate.compareTo(renewalEndDate);
            System.out.println("num: " + num);
            if (num > 0) {
                myDate = initialTermEndDate;
            } else {
                myDate = renewalEndDate;
            }
        } else {
            myDate = initialTermEndDate;
        }

        int oDate;
        System.out.println(Math.round(autoRenewalServicePeriodTerm * 30.41666666666667));
        System.out.println(Math.round(autoRenewalServicePeriodTerm * Constants.CONVERTMONTHTODAY));

        if (dto.getCustomerGroup().equals(Constants.BAU)) {
            if (autoRenewalServicePeriodTerm == 0.001 || autoRenewalServicePeriodTerm  == 0) {
                etlDates.setBillEndDate(this.addDays(dto.getCustomerNotifiedDate(),
                                                     contractData.getErlyTerminationNoticeCust()));
            } else {
                do {
                    autoRenewalEndDate = this.addDays(myDate, (int) (autoRenewalServicePeriodTerm * Constants.CONVERTMONTHTODAY));
                    System.out.println("AutoRenewalEndDate " + autoRenewalEndDate);
                    System.out.println("oDate " + this.addDays(autoRenewalEndDate, -noticePeriod));
                    oDate = this.addDays(autoRenewalEndDate, (-noticePeriod)).compareTo(dto.getCustomerNotifiedDate());
                       System.out.println("oDate: " + oDate);
                    myDate = autoRenewalEndDate;
                    etlDates.setAutoRenewalEndDate(myDate);                            

                } while (oDate < 0);

                etlDates.setBillEndDate(this.addDays(dto.getCustomerNotifiedDate(),
                                                     noticePeriod));
                autoRenewalStartDate = this.addDays(etlDates.getAutoRenewalEndDate(), (int) -(autoRenewalServicePeriodTerm * Constants.CONVERTMONTHTODAY));
                etlDates.setAutoRenewalStartDate(this.addDays(autoRenewalStartDate, 1));
                //etlDates.setAutoRenewalEndDate(this.addDays(etlDates.getAutoRenewalEndDate(), (int) (autoRenewalServicePeriodTerm * Constants.CONVERTMONTHTODAY)));
                System.out.println("Auto Renewal End Date checked: " + etlDates.getAutoRenewalEndDate());
                System.out.println("Auto Renewal Start Date: " + etlDates.getAutoRenewalStartDate());
            }
        } else {
            etlDates.setBillEndDate(dto.getCustomerNotifiedDate());
        }
        return etlDates;
    }

    public Timestamp addDays(Timestamp date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return new Timestamp(cal.getTime().getTime());

    }

    public String generateEtlCode() {
        TIMELOCK.lock();
        long now = System.currentTimeMillis();
        try {
            while (true) {
                long previous = BASETIME.get();
                if (previous >= now) {
                    now = previous + 1;
                }
                if (BASETIME.compareAndSet(previous, now)) {
                    return now + "";
                }
            }
        } finally {
            TIMELOCK.unlock();
        }
    }

    public List<EtlData> getAllEtl() {
        return this.repo.findAll();
    }
}
