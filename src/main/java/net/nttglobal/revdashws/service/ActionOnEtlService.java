package net.nttglobal.revdashws.service;

import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.nttglobal.revdashws.exception.ResourceNotFoundException;
import net.nttglobal.revdashws.model.ActionOnEtl;
import net.nttglobal.revdashws.repository.ActionOnEtlRepository;
import net.nttglobal.revdashws.repository.EtlDataRepository;

@Service
public class ActionOnEtlService {

	@Autowired
	private ActionOnEtlRepository actionRepo;
	
	@Autowired
	private EtlDataRepository etlRepo;
	
	@Autowired
	private EtlDataService etlService;
	
	public ActionOnEtl addAction(String etlCode, ActionOnEtl action) {
		this.etlService.updateEtlStatus(etlCode, action.getAction());
		return this.etlRepo.findByEtlCode(etlCode).stream().map(etlData -> {
			action.setEtlData(etlData);
			action.setActionOn(LocalDateTime.now());
			return this.actionRepo.save(action);
		}).findAny().orElseThrow(() -> new ResourceNotFoundException("ETL", "etlCode", etlCode));
	}
}
